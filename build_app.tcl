#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group


# Synthesize, implement using the static shell and generate bitstreams
# Arguments : [name of the app project]
set src_dir [file normalize "[file dirname [info script]]"]

source "${src_dir}/synth_app_project.tcl"
source "${src_dir}/impl_app.tcl"
source "${src_dir}/write_bitstream_app.tcl"
