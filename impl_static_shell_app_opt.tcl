#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

# Place & route an app linked to a synthesized shell, optimized

source -quiet enzian.conf.tcl

set enzian_shell "shell"
if {[info exists enzian_app] == 0} {
    set enzian_app [lindex $argv 0]
}

if {[info exists build_dir] == 0} {
    set build_dir [file normalize "."]
}
set shell_src_dir   [file normalize "[file dirname [info script]]"]

source -quiet "${shell_src_dir}/impl_functions.tcl"

create_project -in_memory -part xcvu9p-flgb2104-3-e
add_files "${build_dir}/${enzian_shell}_synthed.dcp"
add_files "${build_dir}/${enzian_app}_synthed.dcp"
add_files -fileset [get_filesets constrs_1] "${shell_src_dir}/xdc/static/impl"
add_files -fileset [get_filesets constrs_1] "${shell_src_dir}/eci-transport/xdc/impl"
add_files -fileset [get_filesets constrs_1] "${shell_src_dir}/xdc/app/impl"
add_files -quiet -fileset [get_filesets constrs_1] "${project_dir}/xdc/impl"

set_property SCOPED_TO_CELLS {i_app} [get_files "${build_dir}/${enzian_app}_synthed.dcp"]
link_design -mode default -reconfig_partitions {i_app} -part xcvu9p-flgb2104-3-e -top enzian_shell
write_checkpoint -force "${build_dir}/${enzian_shell}_${enzian_app}_linked.dcp"

opt_design -directive ExploreSequentialArea
power_opt_design
write_checkpoint -force "${build_dir}/${enzian_shell}_${enzian_app}_opted.dcp"

place_design -directive ExtraNetDelay_high
write_checkpoint -force "${build_dir}/${enzian_shell}_${enzian_app}_placed.dcp"

phys_opt_design -directive AlternateFlowWithRetiming
phys_opt_design -directive AggressiveFanoutOpt
phys_opt_design -directive AggressiveExplore
phys_opt_design -directive AlternateReplication
write_checkpoint -force "${build_dir}/${enzian_shell}_${enzian_app}_placed_phys_opted.dcp"

route_design -directive AggressiveExplore
write_checkpoint -force "${build_dir}/${enzian_shell}_${enzian_app}_routed_pre_phys_opted.dcp"
phys_opt_design -directive AggressiveExplore
write_checkpoint -force "${build_dir}/${enzian_shell}_${enzian_app}_routed.dcp"

report_utilization -file "${build_dir}/${enzian_shell}_${enzian_app}_utilization.rpt"
report_timing_summary -file "${build_dir}/${enzian_shell}_${enzian_app}_timing_summary.rpt"
update_design -cell i_app -black_box
lock_design -level routing
write_checkpoint -force "${build_dir}/static_${enzian_shell}_routed.dcp"
close_project
