#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

set_msg_config -suppress -id {[DRC RPBF-3] IO port buffering is incomplete}
set_msg_config -suppress -id {[Constraints 18-4866] Site}
set_msg_config -suppress -id {[Vivado 12-2261] HD.PARTPIN_RANGE cannot be set on pins}
set_msg_config -suppress -id {[Vivado 12-4385] Failed to assign PARTPIN_RANGE on port}
set_msg_config -suppress -id {[Project 1-486] Could not resolve non-primitive black box cell 'enzian_app' instantiated as 'i_app'}
