#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

# Synthesize a shell

source -quiet enzian.conf.tcl

set shell_project "shell"
set build_dir [file normalize "."]

open_project "${build_dir}/${shell_project}/${shell_project}.xpr"
set_property -name {STEPS.SYNTH_DESIGN.ARGS.DIRECTIVE} -value {LogicCompaction} -objects [get_runs synth_1]

# Set the shell version to the git short hash
set git_version [exec git -c core.abbrev=8 rev-parse --short HEAD]
set_property generic SHELL_VERSION=$git_version [current_fileset]

update_compile_order
reset_run synth_1
launch_runs -jobs 8 -verbose synth_1
wait_on_run synth_1
open_run synth_1
write_checkpoint -force "${build_dir}/${shell_project}_synthed.dcp"
report_utilization -file "${build_dir}/${shell_project}_synthed.rpt"
close_project
