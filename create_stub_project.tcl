#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group


set project     "stub"
set project_dir "dynamic-stub"
set part        "xcvu9p-flgb2104-3-e"
#set board_part  "eth.ch:enzian:1.0"

# IP location must be passed as the first argument.
set ip_dir [lindex $argv 0]

# Source files are included relative to the directory containing this script.
set shell_src_dir   [file normalize "[file dirname [info script]]"]
set build_dir [file normalize "."]

# Create project
create_project $project "${build_dir}/${project}" -part $part
set proj [current_project]

# Set project properties
#set_property "board_part" $board_part                   $proj
set_property "default_lib" "xil_defaultlib"                 $proj
set_property "ip_cache_permissions" "read write"            $proj
set_property "ip_output_repo" "${build_dir}/${project}/${project}.cache/ip"  $proj
set_property "sim.ip.auto_export_scripts" "1"               $proj
set_property "simulator_language" "Mixed"                   $proj
set_property "target_language" "VHDL"                       $proj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY"           $proj
set_property "ip_repo_paths" "${ip_dir}"                      $proj
set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value {-mode out_of_context} -objects [get_runs synth_1]

# Make sure any repository IP is visible.
update_ip_catalog

add_files -fileset [get_filesets sources_1] "${shell_src_dir}/${project_dir}/hdl"
add_files -fileset [get_filesets sources_1] "${shell_src_dir}/eci-toolkit/hdl"
# Used only in synthesis
add_files -fileset [get_filesets constrs_1] "${shell_src_dir}/eci-toolkit/xdc"
set_property used_in_implementation false  [get_files -of_objects [get_filesets constrs_1]]

# Set top entity
set_property "top" ${project} [get_filesets sources_1]

# Create a project-local constraint file to take debugging constraints that we
# don't want to propagate to the repository.
file mkdir "${build_dir}/${project}/${project}.srcs/constrs_1"
close [ open "${build_dir}/${project}/${project}.srcs/constrs_1/local.xdc" w ]

# Generate AXI BRAM
create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip \
          -module_name axi_bram_ctrl_0
set_property -dict { \
CONFIG.PROTOCOL {AXI4} \
CONFIG.DATA_WIDTH {64} \
CONFIG.MEM_DEPTH {1024} \
CONFIG.BMG_INSTANCE {INTERNAL} \
CONFIG.SINGLE_PORT_BRAM {1} \
CONFIG.READ_LATENCY {1} \
CONFIG.ID_WIDTH {0} \
CONFIG.SUPPORTS_NARROW_BURST {0} \
} [get_ips axi_bram_ctrl_0]
generate_target all [get_ips axi_bram_ctrl_0]

### Regenerate IP
puts "regenerate IPs"
source "${shell_src_dir}/eci-toolkit/create_ips.tcl"

close_project

# Update a local config file
set conffile [open "enzian.conf.tcl" "a"]
puts $conffile "set enzian_app \"${project}\""
puts $conffile "set project_dir \"${project_dir}\""
close $conffile
