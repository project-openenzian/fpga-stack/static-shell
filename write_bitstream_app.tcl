#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

# Write a bitstream for an app
# The app name is either in the $enzian_app variable or passed as an argument

source -quiet enzian.conf.tcl

if {[info exists enzian_shell] == 0} {
    set enzian_shell "shell"
}
if {[info exists enzian_app] == 0} {
    set enzian_app [lindex $argv 0]
}
if {[info exists build_dir] == 0} {
    set build_dir [file normalize "."]
}

open_checkpoint "${build_dir}/${enzian_shell}_${enzian_app}_routed.dcp"
write_bitstream -force "${build_dir}/${enzian_shell}_${enzian_app}.bit"
write_debug_probes -force "${build_dir}/${enzian_shell}_${enzian_app}.ltx"
close_project
