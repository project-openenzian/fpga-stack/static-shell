#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

# Synthesize an app
# The app name is either in the $enzian_app variable or passed as an argument
# If there's no $build_dir, a local directory is used

source -quiet enzian.conf.tcl

if {[info exists enzian_app] == 0} {
    set enzian_app [lindex $argv 0]
}
if {[info exists build_dir] == 0} {
    set build_dir [file normalize "."]
}

open_project "${build_dir}/${enzian_app}/${enzian_app}.xpr"
# Set the out-of-context-mode, so no port buffers will be instiantiated"
set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value {-mode out_of_context} -objects [get_runs synth_1]
set_property -name {STEPS.SYNTH_DESIGN.ARGS.DIRECTIVE} -value {LogicCompaction} -objects [get_runs synth_1]
update_compile_order
reset_run synth_1
launch_runs -jobs 8 -verbose synth_1
wait_on_run synth_1
open_run synth_1
write_checkpoint -force "${build_dir}/${enzian_app}_synthed.dcp"
report_utilization -file "${build_dir}/${enzian_app}_synthed.rpt"
close_project
