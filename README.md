![]()<img src="doc/img/enzian_logo.svg" alt="Enzian logo" width=120>
# Enzian Shell

## Quick start
 * Clone the repo including the submodules:
  `git clone --recurse-submodules <path-to-the-repo>`
 * Go to the repo
  `cd enzian_shell`
 * Build the shell
  `<path-to-vivado>/bin/vivado -mode batch -source build_static_shell_stub.tcl`
 * Program an Enzian with the bitstream `shell_stub.bit` to test the Enzian out

## What is Enzian?
Enzian is a research computer. It unites a Cavium 48-core ThunderX 1 CPU connected with a Xilinx Ultrascale+ FPGA using a high speed cache coherent link called the ECI - Enzian Coherent Interconnect.

## About the shell
This project builds the initial bitstream for this FPGA as well the static routed design checkpoint (dcp) used to link with a user application. The shell is a loading platform for Enzian applications. It provides the basic ECI functionality, the ECI link handling, the ECI I/O messages handling, the dynamic reconfiguration port (ICAP) and the AXI master and slave buses.

The FPGA is physically split into two regions: static and dynamic (reconfigurable). The shell resides in the static region and the Enzian app in the dynamic region.
The initial bitstream consists of the shell in the static region and the Enzian Stub app in the dynamic region.

Top level block layout:

![](doc/img/enzian_shell_top_level.svg "Enzian top level")

## Requierments
 * Vivado 2022.1

## Repositories
 * Enzian Shell
   * Enzian Stub Application
 * ECI Transport, low-level protocol handling, used by the shell, also containts the ECI Platform module (requires the ECI Toolkit)
 * ECI Toolkit, high-level protocol handling, used by the shell and applications
 
## Building a static shell
All the shell projects, checkpoints etc. will be created in the shell directory.
To create the shell, execute from the shell directory:

`<path-to-vivado>/bin/vivado -mode batch -source build_static_shell_stub.tcl`

or, if you need a version with optimized implementation (takes more time), execute:

`<path-to-vivado>/bin/vivado -mode batch -source build_static_shell_stub_opt.tcl`

The content of the implemented shell (the shell constraints and checkpoints) is later used to implement applications. You specify the location by setting the environment variable `ENZIAN_SHELL_DIR` or by setting the `enzian_shell_dir` variable in your local settings file.

### Application
Each application should contain a local settings file, `enzian.conf.tcl`, in its build directory.
Settings:
 * `enzian_app` - name of an applications, its project directory and its project name and the name of a top level module
 * `project_dir` - specify the project directory if different from the application name
 * `enzian_shell_dir` - location of the shell sources and the shell design checkpoints

### Project workflow
The project flow deviates from a typical Vivado DFX flow. Two projects are created, the shell and the stub. They are separately synthesized and then they are linked together and implemented as a whole.
The scripts produce design checkpoints (.dcp) that can be opened in Vivado and used to inspect the details of synthesis/implementation. There could be two versions of scripts: normal and optimized. The optimized ones try to generate a better implemented design with lower slacks (WNS/TNS) at the expense of duration.

The project workflow:

![](doc/img/enzian_workflow.svg "Enzian project workflow")

### Checkpoints:
 * `shell_synthed.dcp` - a synthesized shell project
 * `stub_synthed.dcp` - a synthesized application project
 * `shell_stub_linked.dcp` - a linked project - the shell with the application
 * `shell_stub_opted.dcp` - an optimized linked project
 * `shell_stub_placed.dcp` - a placed linked project
 * `shell_stub_placed_phys_opted.dcp` - a placed linked project
 * `shell_stub_routed_pre_phys_opted.dcp` - a routed linked project, before physical optimizations
 * `shell_stub_routed.dcp` - a routed linked project, used to generate a bistream
 * `static_shell_routed.dcp` - the static routed shell project, used to link with applications

### Building an custom app
All of the products will be created in the current directory, it can be the source directory or a newly created directory.

### Vivado scripts
The scripts can be executed by calling the Vivado program in tcl mode, for example:
`<path-to-vivado>/bin/vivado -mode batch -source impl_app.tcl`

#### Composed
 * `build_static_shell_stub.tcl` - creates, synthesizes, implements and writes the initial static bitstream
 * `build_static_shell_stub_opt.tcl` - creates, synthesizes, implements and writes the initial static bitstream, optimized place & route
 * `build_static_shell_stub_single.tcl` - same as above, but no multithreading, single thread only to have reproducible builds
 * `build_app.tcl` - creates, synthesizes, implements and writes the bitstreams of the application
 * `build_app_opt.tcl` - creates, synthesizes, implements and writes the bitstreams of the application, optimized place & route

#### Project creation
 * `create_shell_project.tcl` - creates the shell project
 * `create_stub_project.tcl` - creates the stub project
 * `create_app_project.tcl.template` - creates an application project

#### Synthesis
 * `synth_shell_project.tcl` - synthesizes the shell project
 * `synth_app_project.tcl` - synthesizes an application project

#### Implementation
 * `impl_static_shell_app.tcl` - links the shell and an app, implements it to create a static image
 * `impl_static_shell_app_opt.tcl` - links the shell and an app, implements it to create a static image, optimized place & route
 * `impl_static_shell_app_single.tcl` - same as above, but no multithreading, single thread only to have reproducible implementations
 * `impl_app.tcl` - links the static image and an synthesized app and implements it
 * `impl_app_opt.tcl` - links the static image and an synthesized app and implements it, optimized place & route

#### Bitstream generation
 * `write_bitstream_app.tcl` - writes bitstreams and corresponding debug probes definitions (ltx files)
   * `shell_<app_name>.bit` - full bitstream with the shell and the app, for example `shell_stub.bit`
   * `shell_<app_name>_pblock_dynamic_partial.bit` - just the app, used to reconfigure an already programmed FPGA, for example `shell_stub_pblock_dynamic_partial.bit`

## Content
 * `hdl` - the shell sources
 * `xdc` - the shell constraints
   * `static/synth` - used in the shell synthesis
   * `static/impl` - used in in the static shell implementation
   * `app/impl` - used in the app implementation
 * `doc` - documentation
 * Submodules:
   - `eci-toolkit`
   - `eci-transport`
   - `dynamic-stub`

### The ECI protocol overview
The ECI protocol exchanges messages in a form of a list of 64-bit words. The message stream, from two physical links, is split into 14 logical virtual channels, numbered from 0 to 13. Each message can consist of 1, 2, 5, 9, 13 or 17 words (packets). 2-word messages are processed as 2 1-word packets.

Virtual channels:
 * 0 - ECI I/O requests (1 or 2 words)
 * 1 - ECI I/O responses (1 or 2 words)
 * 2,3 - ECI memory requests with payload (5, 9, 13, 17 words)
 * 4,5 - ECI memory responds with payload (5, 9, 13, 17 words)
 * 6,7 - ECI memory requests without payload (1 word)
 * 8,9 - ECI memory forward requests (1 word)
 * 10,11 - ECI memory responds without payload (1 word)
 * 12 - coprocessor messages (1 word)
 * 13 - link discovery messages (1 word)

The virtual channels are split into 2 categories: 2, 3, 4 and 5 are the high bandwidth channels (hi VCs) and others are the low bandwidth channels (lo VCs).

The virtual channels 0, 1 and 13 are handled by the shell. The ECI I/O bridge converts the ECI I/O messages into AXI-lite transactions and handles the link discovery.

ECI messages are passed in the FPGA using the ECI_CHANNEL interface with a valid/ready handshaking style. The interface consists of:
 * data - 9 words, header (data(0)) + 8 words of payload (data(8 downto 0))
 * vc_no - VC number
 * size - packet size and beat number
 * valid - handshaking

Packets can be sent in multiple beats. Packets of size 1, 2, 5 and 9 are transfered in one beat, packets of size 3, 13 and 17 - in two beats, the header always stays the same in all beats.
The list of beats:
 * 1  - 0L
 * 2  - 1L
 * 3  - 1F 1L
 * 4  - 1F 1F 1L
 * 5  - 4L
 * 6  - 4F 1L
 * 7  - 4F 1F 1L
 * 8  - 4F 1F 1F 1L
 * 9  - 8L
 * 10 - 8F 1L
 * 11 - 8F 1F 1L
 * 12 - 8F 1F 1F 1L
 * 13 - 8F 4L
 * 14 - 8F 4F 1L
 * 15 - 8F 4F 1F 1L
 * 16 - 8F 4F 1F 1F 1L
 * 17 - 8F 8L
 * 18 - 8F 8F 1L
 * 19 - 8F 8F 1F 1L
 * 20 - 8F 8F 1F 1F 1L

### The application interface
 * Clocks
 * Reset
 * Link status
 * Two ECI links:
   * RX ECI frames - incoming ECI frames. A frame consists of 7 words and its 7 VC numbers.
   * TX ECI frames - outgoing ECI channels
     * Two Lo ECI channels
     * Two Hi ECI channels
 * Master AXI-lite bus - incoming ECI I/O requests from the CPU
 * Slave AXI-lite bus - outgoing ECI I/O requests to the CPU
 * BSCAN interface


