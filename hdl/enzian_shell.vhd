-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity enzian_shell is
generic (
    SHELL_VERSION : string
);
port (
-- 156.25MHz transceiver reference clocks
    F_CCPIC_CLK_P_LINK1 : in std_logic_vector(2 downto 0);
    F_CCPIC_CLK_N_LINK1 : in std_logic_vector(2 downto 0);

    F_CCPIC_CLK_P_LINK2 : in std_logic_vector(5 downto 3);
    F_CCPIC_CLK_N_LINK2 : in std_logic_vector(5 downto 3);
-- free-running clocks
    F_PRGC0_CLK_P   : in std_logic;
    F_PRGC0_CLK_N   : in std_logic;
    F_PRGC1_CLK_P   : in std_logic;
    F_PRGC1_CLK_N   : in std_logic;
-- RX differential pairs
    CCPI_C2F_P_LINK1 : in std_logic_vector(11 downto 0);
    CCPI_C2F_N_LINK1 : in std_logic_vector(11 downto 0);
    CCPI_C2F_P_LINK2 : in std_logic_vector(11 downto 0);
    CCPI_C2F_N_LINK2 : in std_logic_vector(11 downto 0);
-- TX differential pairs
    CCPI_F2C_P_LINK1 : out std_logic_vector(11 downto 0);
    CCPI_F2C_N_LINK1 : out std_logic_vector(11 downto 0);
    CCPI_F2C_P_LINK2 : out std_logic_vector(11 downto 0);
    CCPI_F2C_N_LINK2 : out std_logic_vector(11 downto 0);
-- DDR4
    F_D1_ACT_N : out std_logic;
    F_D1_A : out std_logic_vector ( 17 downto 0 );
    F_D1_BA : out std_logic_vector ( 1 downto 0 );
    F_D1_BG : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D1_CKE : out std_logic_vector ( 1 downto 0 );
    F_D1_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D1_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D1_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D1_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D1_ODT : out std_logic_vector ( 1 downto 0 );
    F_D1_PARITY_N : out std_logic;
    F_D1_RESET_N : out std_logic;
    F_D1C_CLK_N : in std_logic;
    F_D1C_CLK_P : in std_logic;

    F_D2_ACT_N : out std_logic;
    F_D2_A : out std_logic_vector ( 17 downto 0 );
    F_D2_BA : out std_logic_vector ( 1 downto 0 );
    F_D2_BG : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D2_CKE : out std_logic_vector ( 1 downto 0 );
    F_D2_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D2_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D2_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D2_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D2_ODT : out std_logic_vector ( 1 downto 0 );
    F_D2_PARITY_N : out std_logic;
    F_D2_RESET_N : out std_logic;
    F_D2C_CLK_N : in std_logic;
    F_D2C_CLK_P : in std_logic;

    F_D3_ACT_N : out std_logic;
    F_D3_A : out std_logic_vector ( 17 downto 0 );
    F_D3_BA : out std_logic_vector ( 1 downto 0 );
    F_D3_BG : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D3_CKE : out std_logic_vector ( 1 downto 0 );
    F_D3_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D3_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D3_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D3_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D3_ODT : out std_logic_vector ( 1 downto 0 );
    F_D3_PARITY_N : out std_logic;
    F_D3_RESET_N : out std_logic;
    F_D3C_CLK_N : in std_logic;
    F_D3C_CLK_P : in std_logic;

    F_D4_ACT_N : out std_logic;
    F_D4_A : out std_logic_vector ( 17 downto 0 );
    F_D4_BA : out std_logic_vector ( 1 downto 0 );
    F_D4_BG : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D4_CKE : out std_logic_vector ( 1 downto 0 );
    F_D4_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D4_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D4_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D4_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D4_ODT : out std_logic_vector ( 1 downto 0 );
    F_D4_PARITY_N : out std_logic;
    F_D4_RESET_N : out std_logic;
    F_D4C_CLK_N : in std_logic;
    F_D4C_CLK_P : in std_logic;
-- CMAC
    F_MAC0C_CLK_P   : in std_logic;
    F_MAC0C_CLK_N   : in std_logic;
    F_MAC0_TX_P : out std_logic_vector(3 downto 0);
    F_MAC0_TX_N : out std_logic_vector(3 downto 0);
    F_MAC0_RX_P : in std_logic_vector(3 downto 0);
    F_MAC0_RX_N : in std_logic_vector(3 downto 0);

    F_MAC1C_CLK_P   : in std_logic;
    F_MAC1C_CLK_N   : in std_logic;
    F_MAC1_TX_P : out std_logic_vector(3 downto 0);
    F_MAC1_TX_N : out std_logic_vector(3 downto 0);
    F_MAC1_RX_P : in std_logic_vector(3 downto 0);
    F_MAC1_RX_N : in std_logic_vector(3 downto 0);

    F_MAC2C_CLK_P   : in std_logic;
    F_MAC2C_CLK_N   : in std_logic;
    F_MAC2_TX_P : out std_logic_vector(3 downto 0);
    F_MAC2_TX_N : out std_logic_vector(3 downto 0);
    F_MAC2_RX_P : in std_logic_vector(3 downto 0);
    F_MAC2_RX_N : in std_logic_vector(3 downto 0);

    F_MAC3C_CLK_P   : in std_logic;
    F_MAC3C_CLK_N   : in std_logic;
    F_MAC3_TX_P : out std_logic_vector(3 downto 0);
    F_MAC3_TX_N : out std_logic_vector(3 downto 0);
    F_MAC3_RX_P : in std_logic_vector(3 downto 0);
    F_MAC3_RX_N : in std_logic_vector(3 downto 0);
-- PCIE x16
    F_PCIE16C_CLK_P   : in std_logic;
    F_PCIE16C_CLK_N   : in std_logic;
    F_PCIE16_TX_P : out std_logic_vector(15 downto 0);
    F_PCIE16_TX_N : out std_logic_vector(15 downto 0);
    F_PCIE16_RX_P : in std_logic_vector(15 downto 0);
    F_PCIE16_RX_N : in std_logic_vector(15 downto 0);
-- NVMe
    F_NVMEC_CLK_P   : in std_logic;
    F_NVMEC_CLK_N   : in std_logic;
    F_NVME_TX_P : out std_logic_vector(3 downto 0);
    F_NVME_TX_N : out std_logic_vector(3 downto 0);
    F_NVME_RX_P : in std_logic_vector(3 downto 0);
    F_NVME_RX_N : in std_logic_vector(3 downto 0);
-- C2C
    B_C2CC_CLK_P    : in std_logic;
    B_C2CC_CLK_N    : in std_logic;
    B_C2C_TX_P      : in std_logic_vector(0 downto 0);
    B_C2C_TX_N      : in std_logic_vector(0 downto 0);
    B_C2C_RX_P      : out std_logic_vector(0 downto 0);
    B_C2C_RX_N      : out std_logic_vector(0 downto 0);
    B_C2C_NMI       : in std_logic;
-- I2C
    F_I2C0_SDA      : inout std_logic;
    F_I2C0_SCL      : inout std_logic;

    F_I2C1_SDA      : inout std_logic;
    F_I2C1_SCL      : inout std_logic;

    F_I2C2_SDA      : inout std_logic;
    F_I2C2_SCL      : inout std_logic;

    F_I2C3_SDA      : inout std_logic;
    F_I2C3_SCL      : inout std_logic;

    F_I2C4_SDA      : inout std_logic;
    F_I2C4_SCL      : inout std_logic;

    F_I2C5_SDA      : inout std_logic;
    F_I2C5_SCL      : inout std_logic;
    F_I2C5_RESET_N  : out std_logic;
    F_I2C5_INT_N    : in std_logic;
-- FUART
    B_FUART_TXD     : in std_logic;
    B_FUART_RXD     : out std_logic;
    B_FUART_RTS     : in std_logic;
    B_FUART_CTS     : out std_logic;
-- IRQ
    F_IRQ_IRQ0      : out std_logic;
    F_IRQ_IRQ1      : out std_logic;
    F_IRQ_IRQ2      : out std_logic;
    F_IRQ_IRQ3      : out std_logic
);

attribute io_buffer_type : string;
attribute io_buffer_type of F_PRGC0_CLK_P: signal is "none";
attribute io_buffer_type of F_PRGC0_CLK_N: signal is "none";
attribute io_buffer_type of F_PRGC1_CLK_P: signal is "none";
attribute io_buffer_type of F_PRGC1_CLK_N: signal is "none";

attribute io_buffer_type of F_D1_ACT_N : signal is "none";
attribute io_buffer_type of F_D1_A : signal is "none";
attribute io_buffer_type of F_D1_BA : signal is "none";
attribute io_buffer_type of F_D1_BG : signal is "none";
attribute io_buffer_type of F_D1_CK_N : signal is "none";
attribute io_buffer_type of F_D1_CK_P : signal is "none";
attribute io_buffer_type of F_D1_CKE : signal is "none";
attribute io_buffer_type of F_D1_CS_N : signal is "none";
attribute io_buffer_type of F_D1_DQ : signal is "none";
attribute io_buffer_type of F_D1_DQS_N : signal is "none";
attribute io_buffer_type of F_D1_DQS_P : signal is "none";
attribute io_buffer_type of F_D1_ODT : signal is "none";
attribute io_buffer_type of F_D1_PARITY_N : signal is "none";
attribute io_buffer_type of F_D1_RESET_N : signal is "none";
attribute io_buffer_type of F_D1C_CLK_N : signal is "none";
attribute io_buffer_type of F_D1C_CLK_P : signal is "none";

attribute io_buffer_type of F_D2_ACT_N : signal is "none";
attribute io_buffer_type of F_D2_A : signal is "none";
attribute io_buffer_type of F_D2_BA : signal is "none";
attribute io_buffer_type of F_D2_BG : signal is "none";
attribute io_buffer_type of F_D2_CK_N : signal is "none";
attribute io_buffer_type of F_D2_CK_P : signal is "none";
attribute io_buffer_type of F_D2_CKE : signal is "none";
attribute io_buffer_type of F_D2_CS_N : signal is "none";
attribute io_buffer_type of F_D2_DQ : signal is "none";
attribute io_buffer_type of F_D2_DQS_N : signal is "none";
attribute io_buffer_type of F_D2_DQS_P : signal is "none";
attribute io_buffer_type of F_D2_ODT : signal is "none";
attribute io_buffer_type of F_D2_PARITY_N : signal is "none";
attribute io_buffer_type of F_D2_RESET_N : signal is "none";
attribute io_buffer_type of F_D2C_CLK_N : signal is "none";
attribute io_buffer_type of F_D2C_CLK_P : signal is "none";

attribute io_buffer_type of F_D3_ACT_N : signal is "none";
attribute io_buffer_type of F_D3_A : signal is "none";
attribute io_buffer_type of F_D3_BA : signal is "none";
attribute io_buffer_type of F_D3_BG : signal is "none";
attribute io_buffer_type of F_D3_CK_N : signal is "none";
attribute io_buffer_type of F_D3_CK_P : signal is "none";
attribute io_buffer_type of F_D3_CKE : signal is "none";
attribute io_buffer_type of F_D3_CS_N : signal is "none";
attribute io_buffer_type of F_D3_DQ : signal is "none";
attribute io_buffer_type of F_D3_DQS_N : signal is "none";
attribute io_buffer_type of F_D3_DQS_P : signal is "none";
attribute io_buffer_type of F_D3_ODT : signal is "none";
attribute io_buffer_type of F_D3_PARITY_N : signal is "none";
attribute io_buffer_type of F_D3_RESET_N : signal is "none";
attribute io_buffer_type of F_D3C_CLK_N : signal is "none";
attribute io_buffer_type of F_D3C_CLK_P : signal is "none";

attribute io_buffer_type of F_D4_ACT_N : signal is "none";
attribute io_buffer_type of F_D4_A : signal is "none";
attribute io_buffer_type of F_D4_BA : signal is "none";
attribute io_buffer_type of F_D4_BG : signal is "none";
attribute io_buffer_type of F_D4_CK_N : signal is "none";
attribute io_buffer_type of F_D4_CK_P : signal is "none";
attribute io_buffer_type of F_D4_CKE : signal is "none";
attribute io_buffer_type of F_D4_CS_N : signal is "none";
attribute io_buffer_type of F_D4_DQ : signal is "none";
attribute io_buffer_type of F_D4_DQS_N : signal is "none";
attribute io_buffer_type of F_D4_DQS_P : signal is "none";
attribute io_buffer_type of F_D4_ODT : signal is "none";
attribute io_buffer_type of F_D4_PARITY_N : signal is "none";
attribute io_buffer_type of F_D4_RESET_N : signal is "none";
attribute io_buffer_type of F_D4C_CLK_N : signal is "none";
attribute io_buffer_type of F_D4C_CLK_P : signal is "none";

attribute io_buffer_type of F_MAC0C_CLK_P : signal is "none";
attribute io_buffer_type of F_MAC0C_CLK_N : signal is "none";
attribute io_buffer_type of F_MAC0_TX_P : signal is "none";
attribute io_buffer_type of F_MAC0_TX_N : signal is "none";
attribute io_buffer_type of F_MAC0_RX_P : signal is "none";
attribute io_buffer_type of F_MAC0_RX_N : signal is "none";

attribute io_buffer_type of F_MAC1C_CLK_P : signal is "none";
attribute io_buffer_type of F_MAC1C_CLK_N : signal is "none";
attribute io_buffer_type of F_MAC1_TX_P : signal is "none";
attribute io_buffer_type of F_MAC1_TX_N : signal is "none";
attribute io_buffer_type of F_MAC1_RX_P : signal is "none";
attribute io_buffer_type of F_MAC1_RX_N : signal is "none";

attribute io_buffer_type of F_MAC2C_CLK_P : signal is "none";
attribute io_buffer_type of F_MAC2C_CLK_N : signal is "none";
attribute io_buffer_type of F_MAC2_TX_P : signal is "none";
attribute io_buffer_type of F_MAC2_TX_N : signal is "none";
attribute io_buffer_type of F_MAC2_RX_P : signal is "none";
attribute io_buffer_type of F_MAC2_RX_N : signal is "none";

attribute io_buffer_type of F_MAC3C_CLK_P : signal is "none";
attribute io_buffer_type of F_MAC3C_CLK_N : signal is "none";
attribute io_buffer_type of F_MAC3_TX_P : signal is "none";
attribute io_buffer_type of F_MAC3_TX_N : signal is "none";
attribute io_buffer_type of F_MAC3_RX_P : signal is "none";
attribute io_buffer_type of F_MAC3_RX_N : signal is "none";

attribute io_buffer_type of F_PCIE16C_CLK_P : signal is "none";
attribute io_buffer_type of F_PCIE16C_CLK_N : signal is "none";
attribute io_buffer_type of F_PCIE16_TX_P : signal is "none";
attribute io_buffer_type of F_PCIE16_TX_N : signal is "none";
attribute io_buffer_type of F_PCIE16_RX_P : signal is "none";
attribute io_buffer_type of F_PCIE16_RX_N : signal is "none";

attribute io_buffer_type of F_NVMEC_CLK_P : signal is "none";
attribute io_buffer_type of F_NVMEC_CLK_N : signal is "none";
attribute io_buffer_type of F_NVME_TX_P : signal is "none";
attribute io_buffer_type of F_NVME_TX_N : signal is "none";
attribute io_buffer_type of F_NVME_RX_P : signal is "none";
attribute io_buffer_type of F_NVME_RX_N     : signal is "none";

attribute io_buffer_type of B_C2CC_CLK_P    : signal is "none";
attribute io_buffer_type of B_C2CC_CLK_N    : signal is "none";
attribute io_buffer_type of B_C2C_TX_P      : signal is "none";
attribute io_buffer_type of B_C2C_TX_N      : signal is "none";
attribute io_buffer_type of B_C2C_RX_P      : signal is "none";
attribute io_buffer_type of B_C2C_RX_N      : signal is "none";
attribute io_buffer_type of B_C2C_NMI       : signal is "none";

attribute io_buffer_type of F_I2C0_SDA      : signal is "none";
attribute io_buffer_type of F_I2C0_SCL      : signal is "none";

attribute io_buffer_type of F_I2C1_SDA      : signal is "none";
attribute io_buffer_type of F_I2C1_SCL      : signal is "none";

attribute io_buffer_type of F_I2C2_SDA      : signal is "none";
attribute io_buffer_type of F_I2C2_SCL      : signal is "none";

attribute io_buffer_type of F_I2C3_SDA      : signal is "none";
attribute io_buffer_type of F_I2C3_SCL      : signal is "none";

attribute io_buffer_type of F_I2C4_SDA      : signal is "none";
attribute io_buffer_type of F_I2C4_SCL      : signal is "none";

attribute io_buffer_type of F_I2C5_SDA      : signal is "none";
attribute io_buffer_type of F_I2C5_SCL      : signal is "none";
attribute io_buffer_type of F_I2C5_RESET_N  : signal is "none";
attribute io_buffer_type of F_I2C5_INT_N    : signal is "none";

attribute io_buffer_type of B_FUART_TXD     : signal is "none";
attribute io_buffer_type of B_FUART_RXD     : signal is "none";
attribute io_buffer_type of B_FUART_RTS     : signal is "none";
attribute io_buffer_type of B_FUART_CTS     : signal is "none";

attribute io_buffer_type of F_IRQ_IRQ0      : signal is "none";
attribute io_buffer_type of F_IRQ_IRQ1      : signal is "none";
attribute io_buffer_type of F_IRQ_IRQ2      : signal is "none";
attribute io_buffer_type of F_IRQ_IRQ3      : signal is "none";

end enzian_shell;

architecture behavioural of enzian_shell is

component eci_platform is
generic (
    SHELL_VERSION : string
);
port (
    clk_sys         : out std_logic;
    clk_icap        : out std_logic;
    reset_sys       : out std_logic;

    -- 156.25MHz transceiver reference clocks
    eci_gt_clk_p_link1  : in std_logic_vector(2 downto 0);
    eci_gt_clk_n_link1  : in std_logic_vector(2 downto 0);

    eci_gt_clk_p_link2  : in std_logic_vector(5 downto 3);
    eci_gt_clk_n_link2  : in std_logic_vector(5 downto 3);

    -- RX differential pairs
    eci_gt_rx_p_link1   : in std_logic_vector(11 downto 0);
    eci_gt_rx_n_link1   : in std_logic_vector(11 downto 0);
    eci_gt_rx_p_link2   : in std_logic_vector(11 downto 0);
    eci_gt_rx_n_link2   : in std_logic_vector(11 downto 0);

    -- TX differential pairs
    eci_gt_tx_p_link1   : out std_logic_vector(11 downto 0);
    eci_gt_tx_n_link1   : out std_logic_vector(11 downto 0);
    eci_gt_tx_p_link2   : out std_logic_vector(11 downto 0);
    eci_gt_tx_n_link2   : out std_logic_vector(11 downto 0);

    link1_in_data           : out WORDS(6 downto 0);
    link1_in_vc_no          : out VCS(6 downto 0);
    link1_in_we2            : out std_logic_vector(6 downto 0);
    link1_in_we3            : out std_logic_vector(6 downto 0);
    link1_in_we4            : out std_logic_vector(6 downto 0);
    link1_in_we5            : out std_logic_vector(6 downto 0);
    link1_in_valid          : out std_logic;
    link1_in_credit_return  : in std_logic_vector(12 downto 2);
    link1_out_hi_vc         : in ECI_CHANNEL;
    link1_out_hi_vc_ready   : out std_logic;
    link1_out_lo_vc         : in ECI_CHANNEL;
    link1_out_lo_vc_ready   : out std_logic;
    link1_out_credit_return : out std_logic_vector(12 downto 2);

    link2_in_data           : out WORDS(6 downto 0);
    link2_in_vc_no          : out VCS(6 downto 0);
    link2_in_we2            : out std_logic_vector(6 downto 0);
    link2_in_we3            : out std_logic_vector(6 downto 0);
    link2_in_we4            : out std_logic_vector(6 downto 0);
    link2_in_we5            : out std_logic_vector(6 downto 0);
    link2_in_valid          : out std_logic;
    link2_in_credit_return  : in std_logic_vector(12 downto 2);
    link2_out_hi_vc         : in ECI_CHANNEL;
    link2_out_hi_vc_ready   : out std_logic;
    link2_out_lo_vc         : in ECI_CHANNEL;
    link2_out_lo_vc_ready   : out std_logic;
    link2_out_credit_return : out std_logic_vector(12 downto 2);

    link_up                 : out std_logic;
    link1_link_up           : out std_logic;
    link2_link_up           : out std_logic;

    disable_2nd_link        : in std_logic;

    -- AXI Lite master interface IO addr space
    m_io_axil_awaddr    : out std_logic_vector(43 downto 0);
    m_io_axil_awvalid   : buffer std_logic;
    m_io_axil_awready   : in  std_logic;

    m_io_axil_wdata     : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb     : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid    : buffer std_logic;
    m_io_axil_wready    : in  std_logic;

    m_io_axil_bresp     : in  std_logic_vector(1 downto 0);
    m_io_axil_bvalid    : in  std_logic;
    m_io_axil_bready    : buffer std_logic;

    m_io_axil_araddr    : out std_logic_vector(43 downto 0);
    m_io_axil_arvalid   : buffer std_logic;
    m_io_axil_arready   : in  std_logic;

    m_io_axil_rdata     : in std_logic_vector(63 downto 0);
    m_io_axil_rresp     : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid    : in std_logic;
    m_io_axil_rready    : buffer std_logic;

    -- AXI Lite slave interface IO addr space
    s_io_axil_awaddr    : in std_logic_vector(43 downto 0);
    s_io_axil_awvalid   : in std_logic;
    s_io_axil_awready   : out std_logic;
    s_io_axil_wdata     : in std_logic_vector(63 downto 0);
    s_io_axil_wstrb     : in std_logic_vector(7 downto 0);
    s_io_axil_wvalid    : in std_logic;
    s_io_axil_wready    : out std_logic;
    s_io_axil_bresp     : out std_logic_vector(1 downto 0);
    s_io_axil_bvalid    : out std_logic;
    s_io_axil_bready    : in std_logic;
    s_io_axil_araddr    : in std_logic_vector(43 downto 0);
    s_io_axil_arvalid   : in std_logic;
    s_io_axil_arready   : out  std_logic;
    s_io_axil_rdata     : out std_logic_vector(63 downto 0);
    s_io_axil_rresp     : out std_logic_vector(1 downto 0);
    s_io_axil_rvalid    : out std_logic;
    s_io_axil_rready    : in std_logic;

    -- ICAP AXI Lite master interface
    m_icap_axi_awaddr   : out std_logic_vector(8 downto 0);
    m_icap_axi_awvalid  : buffer std_logic;
    m_icap_axi_awready  : in  std_logic;

    m_icap_axi_wdata    : out std_logic_vector(31 downto 0);
    m_icap_axi_wstrb    : out std_logic_vector(3 downto 0);
    m_icap_axi_wvalid   : buffer std_logic;
    m_icap_axi_wready   : in  std_logic;

    m_icap_axi_bresp    : in  std_logic_vector(1 downto 0);
    m_icap_axi_bvalid   : in  std_logic;
    m_icap_axi_bready   : buffer std_logic;

    m_icap_axi_araddr   : out std_logic_vector(8 downto 0);
    m_icap_axi_arvalid  : buffer std_logic;
    m_icap_axi_arready  : in  std_logic;

    m_icap_axi_rdata    : in std_logic_vector(31 downto 0);
    m_icap_axi_rresp    : in std_logic_vector(1 downto 0);
    m_icap_axi_rvalid   : in std_logic;
    m_icap_axi_rready   : buffer std_logic;

    shell_status_reg    : in std_logic_vector(63 downto 0);
    shell_control_reg   : out std_logic_vector(63 downto 0);

    gpo_regs            : out WORDS(15 downto 0);
    gpi_regs            : in WORDS(15 downto 0)
);
end component;

component enzian_app is
port (
-- 322.265625 MHz
    clk_sys                 : in std_logic;
-- programmed to 100MHz
    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
-- programmed to 300MHz
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;
-- power on reset
    reset_sys               : in std_logic;
-- ECI link status
    link1_up                : in std_logic;
    link2_up                : in std_logic;
-- ECI links
    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);

    disable_2nd_link        : out std_logic;
-- AXI Lite FPGA -> CPU
    m_io_axil_awaddr        : out std_logic_vector(43 downto 0);
    m_io_axil_awvalid       : out std_logic;
    m_io_axil_awready       : in std_logic;
    m_io_axil_wdata         : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb         : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid        : out std_logic;
    m_io_axil_wready        : in std_logic;
    m_io_axil_bresp         : in std_logic_vector(1 downto 0);
    m_io_axil_bvalid        : in std_logic;
    m_io_axil_bready        : out std_logic;
    m_io_axil_araddr        : out std_logic_vector(43 downto 0);
    m_io_axil_arvalid       : out std_logic;
    m_io_axil_arready       : in  std_logic;
    m_io_axil_rdata         : in std_logic_vector(63 downto 0);
    m_io_axil_rresp         : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid        : in std_logic;
    m_io_axil_rready        : out std_logic;
-- AXI Lite CPU -> FPGA
    s_io_axil_awaddr        : in std_logic_vector(43 downto 0);
    s_io_axil_awvalid       : in std_logic;
    s_io_axil_awready       : out std_logic;
    s_io_axil_wdata         : in std_logic_vector(63 downto 0);
    s_io_axil_wstrb         : in std_logic_vector(7 downto 0);
    s_io_axil_wvalid        : in std_logic;
    s_io_axil_wready        : out std_logic;
    s_io_axil_bresp         : out std_logic_vector(1 downto 0);
    s_io_axil_bvalid        : out std_logic;
    s_io_axil_bready        : in std_logic;
    s_io_axil_araddr        : in std_logic_vector(43 downto 0);
    s_io_axil_arvalid       : in std_logic;
    s_io_axil_arready       : out  std_logic;
    s_io_axil_rdata         : out std_logic_vector(63 downto 0);
    s_io_axil_rresp         : out std_logic_vector(1 downto 0);
    s_io_axil_rvalid        : out std_logic;
    s_io_axil_rready        : in std_logic;
-- BSCAN slave port for ILAs, VIOs, MIGs, MDMs etc.
    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;
-- Microblaze Debug Module port
    mdm_SYS_Rst             : in std_logic;
    mdm_Clk                 : in std_logic;
    mdm_TDI                 : in std_logic;
    mdm_TDO                 : out std_logic;
    mdm_Reg_En              : in std_logic_vector(0 to 7);
    mdm_Capture             : in std_logic;
    mdm_Shift               : in std_logic;
    mdm_Update              : in std_logic;
    mdm_Rst                 : in std_logic;
    mdm_Disable             : in std_logic;
-- general purpose registers, accessible through the I/O space
    gpo_reg0            : in std_logic_vector(63 downto 0);
    gpo_reg1            : in std_logic_vector(63 downto 0);
    gpo_reg2            : in std_logic_vector(63 downto 0);
    gpo_reg3            : in std_logic_vector(63 downto 0);
    gpo_reg4            : in std_logic_vector(63 downto 0);
    gpo_reg5            : in std_logic_vector(63 downto 0);
    gpo_reg6            : in std_logic_vector(63 downto 0);
    gpo_reg7            : in std_logic_vector(63 downto 0);
    gpo_reg8            : in std_logic_vector(63 downto 0);
    gpo_reg9            : in std_logic_vector(63 downto 0);
    gpo_reg10           : in std_logic_vector(63 downto 0);
    gpo_reg11           : in std_logic_vector(63 downto 0);
    gpo_reg12           : in std_logic_vector(63 downto 0);
    gpo_reg13           : in std_logic_vector(63 downto 0);
    gpo_reg14           : in std_logic_vector(63 downto 0);
    gpo_reg15           : in std_logic_vector(63 downto 0);
    gpi_reg0            : out std_logic_vector(63 downto 0);
    gpi_reg1            : out std_logic_vector(63 downto 0);
    gpi_reg2            : out std_logic_vector(63 downto 0);
    gpi_reg3            : out std_logic_vector(63 downto 0);
    gpi_reg4            : out std_logic_vector(63 downto 0);
    gpi_reg5            : out std_logic_vector(63 downto 0);
    gpi_reg6            : out std_logic_vector(63 downto 0);
    gpi_reg7            : out std_logic_vector(63 downto 0);
    gpi_reg8            : out std_logic_vector(63 downto 0);
    gpi_reg9            : out std_logic_vector(63 downto 0);
    gpi_reg10           : out std_logic_vector(63 downto 0);
    gpi_reg11           : out std_logic_vector(63 downto 0);
    gpi_reg12           : out std_logic_vector(63 downto 0);
    gpi_reg13           : out std_logic_vector(63 downto 0);
    gpi_reg14           : out std_logic_vector(63 downto 0);
    gpi_reg15           : out std_logic_vector(63 downto 0);
-- DDR4
    F_D1_ACT_N : out std_logic;
    F_D1_A : out std_logic_vector ( 17 downto 0 );
    F_D1_BA : out std_logic_vector ( 1 downto 0 );
    F_D1_BG : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D1_CKE : out std_logic_vector ( 1 downto 0 );
    F_D1_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D1_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D1_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D1_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D1_ODT : out std_logic_vector ( 1 downto 0 );
    F_D1_PARITY_N : out std_logic;
    F_D1_RESET_N : out std_logic;
    F_D1C_CLK_N : in std_logic;
    F_D1C_CLK_P : in std_logic;

    F_D2_ACT_N : out std_logic;
    F_D2_A : out std_logic_vector ( 17 downto 0 );
    F_D2_BA : out std_logic_vector ( 1 downto 0 );
    F_D2_BG : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D2_CKE : out std_logic_vector ( 1 downto 0 );
    F_D2_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D2_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D2_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D2_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D2_ODT : out std_logic_vector ( 1 downto 0 );
    F_D2_PARITY_N : out std_logic;
    F_D2_RESET_N : out std_logic;
    F_D2C_CLK_N : in std_logic;
    F_D2C_CLK_P : in std_logic;

    F_D3_ACT_N : out std_logic;
    F_D3_A : out std_logic_vector ( 17 downto 0 );
    F_D3_BA : out std_logic_vector ( 1 downto 0 );
    F_D3_BG : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D3_CKE : out std_logic_vector ( 1 downto 0 );
    F_D3_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D3_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D3_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D3_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D3_ODT : out std_logic_vector ( 1 downto 0 );
    F_D3_PARITY_N : out std_logic;
    F_D3_RESET_N : out std_logic;
    F_D3C_CLK_N : in std_logic;
    F_D3C_CLK_P : in std_logic;

    F_D4_ACT_N : out std_logic;
    F_D4_A : out std_logic_vector ( 17 downto 0 );
    F_D4_BA : out std_logic_vector ( 1 downto 0 );
    F_D4_BG : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D4_CKE : out std_logic_vector ( 1 downto 0 );
    F_D4_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D4_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D4_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D4_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D4_ODT : out std_logic_vector ( 1 downto 0 );
    F_D4_PARITY_N : out std_logic;
    F_D4_RESET_N : out std_logic;
    F_D4C_CLK_N : in std_logic;
    F_D4C_CLK_P : in std_logic;
-- CMAC
    F_MAC0C_CLK_P   : in std_logic;
    F_MAC0C_CLK_N   : in std_logic;
    F_MAC0_TX_P : out std_logic_vector(3 downto 0);
    F_MAC0_TX_N : out std_logic_vector(3 downto 0);
    F_MAC0_RX_P : in std_logic_vector(3 downto 0);
    F_MAC0_RX_N : in std_logic_vector(3 downto 0);

    F_MAC1C_CLK_P   : in std_logic;
    F_MAC1C_CLK_N   : in std_logic;
    F_MAC1_TX_P : out std_logic_vector(3 downto 0);
    F_MAC1_TX_N : out std_logic_vector(3 downto 0);
    F_MAC1_RX_P : in std_logic_vector(3 downto 0);
    F_MAC1_RX_N : in std_logic_vector(3 downto 0);

    F_MAC2C_CLK_P   : in std_logic;
    F_MAC2C_CLK_N   : in std_logic;
    F_MAC2_TX_P : out std_logic_vector(3 downto 0);
    F_MAC2_TX_N : out std_logic_vector(3 downto 0);
    F_MAC2_RX_P : in std_logic_vector(3 downto 0);
    F_MAC2_RX_N : in std_logic_vector(3 downto 0);

    F_MAC3C_CLK_P   : in std_logic;
    F_MAC3C_CLK_N   : in std_logic;
    F_MAC3_TX_P : out std_logic_vector(3 downto 0);
    F_MAC3_TX_N : out std_logic_vector(3 downto 0);
    F_MAC3_RX_P : in std_logic_vector(3 downto 0);
    F_MAC3_RX_N : in std_logic_vector(3 downto 0);
-- PCIE x16
    F_PCIE16C_CLK_P   : in std_logic;
    F_PCIE16C_CLK_N   : in std_logic;
    F_PCIE16_TX_P : out std_logic_vector(15 downto 0);
    F_PCIE16_TX_N : out std_logic_vector(15 downto 0);
    F_PCIE16_RX_P : in std_logic_vector(15 downto 0);
    F_PCIE16_RX_N : in std_logic_vector(15 downto 0);
-- NVMe
    F_NVMEC_CLK_P   : in std_logic;
    F_NVMEC_CLK_N   : in std_logic;
    F_NVME_TX_P : out std_logic_vector(3 downto 0);
    F_NVME_TX_N : out std_logic_vector(3 downto 0);
    F_NVME_RX_P : in std_logic_vector(3 downto 0);
    F_NVME_RX_N : in std_logic_vector(3 downto 0);
-- C2C
    B_C2CC_CLK_P    : in std_logic;
    B_C2CC_CLK_N    : in std_logic;
    B_C2C_TX_P      : in std_logic_vector(0 downto 0);
    B_C2C_TX_N      : in std_logic_vector(0 downto 0);
    B_C2C_RX_P      : out std_logic_vector(0 downto 0);
    B_C2C_RX_N      : out std_logic_vector(0 downto 0);
    B_C2C_NMI       : in std_logic;
-- I2C
    F_I2C0_SDA      : inout std_logic;
    F_I2C0_SCL      : inout std_logic;

    F_I2C1_SDA      : inout std_logic;
    F_I2C1_SCL      : inout std_logic;

    F_I2C2_SDA      : inout std_logic;
    F_I2C2_SCL      : inout std_logic;

    F_I2C3_SDA      : inout std_logic;
    F_I2C3_SCL      : inout std_logic;

    F_I2C4_SDA      : inout std_logic;
    F_I2C4_SCL      : inout std_logic;

    F_I2C5_SDA      : inout std_logic;
    F_I2C5_SCL      : inout std_logic;
    F_I2C5_RESET_N  : out std_logic;
    F_I2C5_INT_N    : in std_logic;
-- FUART
    B_FUART_TXD     : in std_logic;
    B_FUART_RXD     : out std_logic;
    B_FUART_RTS     : in std_logic;
    B_FUART_CTS     : out std_logic;
-- IRQ
    F_IRQ_IRQ0      : out std_logic;
    F_IRQ_IRQ1      : out std_logic;
    F_IRQ_IRQ2      : out std_logic;
    F_IRQ_IRQ3      : out std_logic
);
end component;

attribute black_box : string;
attribute black_box of enzian_app : component is "yes";

component debug_bridge_static is
port (
    m0_bscan_bscanid_en : out std_logic;
    m0_bscan_capture : out std_logic;
    m0_bscan_drck : out std_logic;
    m0_bscan_reset : out std_logic;
    m0_bscan_runtest : out std_logic;
    m0_bscan_sel : out std_logic;
    m0_bscan_shift : out std_logic;
    m0_bscan_tck : out std_logic;
    m0_bscan_tdi : out std_logic;
    m0_bscan_tdo : in std_logic;
    m0_bscan_tms : out std_logic;
    m0_bscan_update : out std_logic;

    m1_bscan_bscanid_en : out std_logic;
    m1_bscan_capture : out std_logic;
    m1_bscan_drck : out std_logic;
    m1_bscan_reset : out std_logic;
    m1_bscan_runtest : out std_logic;
    m1_bscan_sel : out std_logic;
    m1_bscan_shift : out std_logic;
    m1_bscan_tck : out std_logic;
    m1_bscan_tdi : out std_logic;
    m1_bscan_tdo : in std_logic;
    m1_bscan_tms : out std_logic;
    m1_bscan_update : out std_logic
);
end component;

component debug_hub_static is
port (
    clk : in std_logic;
    s_bscan_bscanid_en : in std_logic;
    s_bscan_capture : in std_logic;
    s_bscan_drck : in std_logic;
    s_bscan_reset : in std_logic;
    s_bscan_runtest : in std_logic;
    s_bscan_sel : in std_logic;
    s_bscan_shift : in std_logic;
    s_bscan_tck : in std_logic;
    s_bscan_tdi : in std_logic;
    s_bscan_tdo : out std_logic;
    s_bscan_tms : in std_logic;
    s_bscan_update : in std_logic
);
end component;

component mdm_0 is
port (
    Debug_SYS_Rst : out std_logic;
    Dbg_Clk_0 : out std_logic;
    Dbg_TDI_0 : out std_logic;
    Dbg_TDO_0 : in std_logic;
    Dbg_Reg_En_0 : out std_logic_vector(0 to 7);
    Dbg_Capture_0 : out std_logic;
    Dbg_Shift_0 : out std_logic;
    Dbg_Update_0 : out std_logic;
    Dbg_Rst_0 : out std_logic;
    Dbg_Disable_0 : out std_logic
);
end component;

component axi_hwicap_0 is
port (
    icap_clk : in std_logic;
    eos_in : in std_logic;
    s_axi_aclk : in std_logic;
    s_axi_aresetn : in std_logic;
    s_axi_awaddr : in std_logic_vector(8 downto 0);
    s_axi_awvalid : in std_logic;
    s_axi_awready : out std_logic;
    s_axi_wdata : in std_logic_vector(31 downto 0);
    s_axi_wstrb : in std_logic_vector(3 downto 0);
    s_axi_wvalid : in std_logic;
    s_axi_wready : out std_logic;
    s_axi_bresp : out std_logic_vector(1 downto 0);
    s_axi_bvalid : out std_logic;
    s_axi_bready : in std_logic;
    s_axi_araddr : in std_logic_vector(8 downto 0);
    s_axi_arvalid : in std_logic;
    s_axi_arready : out std_logic;
    s_axi_rdata : out std_logic_vector(31 downto 0);
    s_axi_rresp : out std_logic_vector(1 downto 0);
    s_axi_rvalid : out std_logic;
    s_axi_rready : in std_logic;
    ip2intc_irpt : out std_logic
);
end component;

-------------------------------------------------------------------------------------------------------------
-- RECORDS

type ECI_LINK_TX is record
    hi          : ECI_CHANNEL;
    hi_ready    : std_logic;
    lo          : ECI_CHANNEL;
    lo_ready    : std_logic;
end record ECI_LINK_TX;

type IO_AXI_LITE is record
    awaddr  :  std_logic_vector(43 downto 0);
    awvalid :  std_logic;
    awready :  std_logic;
    wdata   :  std_logic_vector(63 downto 0);
    wstrb   :  std_logic_vector(7 downto 0);
    wvalid  :  std_logic;
    wready  :  std_logic;
    bresp   :  std_logic_vector(1 downto 0);
    bvalid  :  std_logic;
    bready  :  std_logic;
    araddr  :  std_logic_vector(43 downto 0);
    arvalid :  std_logic;
    arready :  std_logic;
    rdata   :  std_logic_vector(63 downto 0);
    rresp   :  std_logic_vector(1 downto 0);
    rvalid  :  std_logic;
    rready  :  std_logic;
end record IO_AXI_LITE;

type ICAP_AXI_LITE is record
    awaddr  :  std_logic_vector(8 downto 0);
    awvalid :  std_logic;
    awready :  std_logic;
    wdata   :  std_logic_vector(31 downto 0);
    wstrb   :  std_logic_vector(3 downto 0);
    wvalid  :  std_logic;
    wready  :  std_logic;
    bresp   :  std_logic_vector(1 downto 0);
    bvalid  :  std_logic;
    bready  :  std_logic;
    araddr  :  std_logic_vector(8 downto 0);
    arvalid :  std_logic;
    arready :  std_logic;
    rdata   :  std_logic_vector(31 downto 0);
    rresp   :  std_logic_vector(1 downto 0);
    rvalid  :  std_logic;
    rready  :  std_logic;
end record ICAP_AXI_LITE;


-- Single-ended, buffered clocks.
signal clk : std_logic;     -- 322MHz, generated by ECI GTY, 156.25MHz * 66 / 32
signal clk_icap : std_logic;  -- 107MHz, clk / 3

signal link1_in_data   : WORDS(6 downto 0);
signal link1_in_vc_no  : VCS(6 downto 0);
signal link1_in_we2    : std_logic_vector(6 downto 0);
signal link1_in_we3    : std_logic_vector(6 downto 0);
signal link1_in_we4    : std_logic_vector(6 downto 0);
signal link1_in_we5    : std_logic_vector(6 downto 0);
signal link1_in_valid  : std_logic;
signal link1_in_credit_return  : std_logic_vector(12 downto 2);
signal link1_in_credit_return_c: std_logic_vector(12 downto 2);
signal link2_in_data   : WORDS(6 downto 0);
signal link2_in_vc_no  : VCS(6 downto 0);
signal link2_in_we2    : std_logic_vector(6 downto 0);
signal link2_in_we3    : std_logic_vector(6 downto 0);
signal link2_in_we4    : std_logic_vector(6 downto 0);
signal link2_in_we5    : std_logic_vector(6 downto 0);
signal link2_in_valid  : std_logic;
signal link2_in_credit_return  : std_logic_vector(12 downto 2);
signal link2_in_credit_return_c: std_logic_vector(12 downto 2);
signal link1_out_credit_return  : std_logic_vector(12 downto 2);
signal link2_out_credit_return  : std_logic_vector(12 downto 2);
signal link1_out_hi_valid_c : std_logic;
signal link1_out_lo_valid_c : std_logic;
signal link2_out_hi_valid_c : std_logic;
signal link2_out_lo_valid_c : std_logic;

signal link1_out, link1_out_cred : ECI_LINK_TX;
signal link2_out, link2_out_cred : ECI_LINK_TX;

signal link1_io         : ECI_CHANNEL;
signal link1_io_ready   : std_logic;
signal link2_io         : ECI_CHANNEL;
signal link2_io_ready   : std_logic;

signal disable_2nd_link : std_logic;
signal decouple_app     : std_logic;

signal m_io_axil_link, s_io_axil_link : IO_AXI_LITE;
signal icap_axil_link : ICAP_AXI_LITE;
signal s_io_axil_link_awvalid_c, s_io_axil_link_wvalid_c, s_io_axil_link_arvalid_c : std_logic;

signal reset, reset_n   : std_logic;
signal reset_app        : std_logic;
signal eci_link_up      : std_logic;
signal link1_eci_link_up    : std_logic;
signal link2_eci_link_up    : std_logic;

-- Shell status/control register
signal shell_status_reg    : std_logic_vector(63 downto 0);
signal shell_control_reg   : std_logic_vector(63 downto 0) := (others => '0');

type BSCAN_INTERFACE is record
    bscanid_en : std_logic;
    capture : std_logic;
    drck : std_logic;
    reset : std_logic;
    runtest : std_logic;
    sel : std_logic;
    shift : std_logic;
    tck : std_logic;
    tdi : std_logic;
    tdo : std_logic;
    tms : std_logic;
    update : std_logic;
end record;

signal m0_bscan, m1_bscan   : BSCAN_INTERFACE;

type MICROBLAZE_DEBUG_INTERFACE is record
    SYS_Rst : std_logic;
    Clk     : std_logic;
    TDI     : std_logic;
    TDO     : std_logic;
    Reg_En  : std_logic_vector(0 to 7);
    Capture : std_logic;
    Shift   : std_logic;
    Update  : std_logic;
    Rst     : std_logic;
    Disable : std_logic;
end record;

signal mdm_port : MICROBLAZE_DEBUG_INTERFACE;

signal gpi_regs, gpo_regs   : WORDS(15 downto 0);

begin

reset_n <= not reset;
shell_status_reg <= shell_control_reg;
decouple_app <= shell_control_reg(0);       -- decouple the APP port
reset_app <= reset or shell_control_reg(1); -- reset the APP

-- Decouple during reconfiguration
link1_in_credit_return <= link1_in_credit_return_c and not (decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app);
link1_out.hi.valid <= link1_out_hi_valid_c and not decouple_app;
link1_out.lo.valid <= link1_out_lo_valid_c and not decouple_app;
link2_in_credit_return <= link2_in_credit_return_c and not (decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app & decouple_app);
link2_out.hi.valid <= link2_out_hi_valid_c and not decouple_app;
link2_out.lo.valid <= link2_out_lo_valid_c and not decouple_app;

s_io_axil_link.awvalid <= s_io_axil_link_awvalid_c and not decouple_app;
s_io_axil_link.wvalid <= s_io_axil_link_wvalid_c and not decouple_app;
s_io_axil_link.arvalid <= s_io_axil_link_arvalid_c and not decouple_app;

i_eci_platform : eci_platform
generic map (
    SHELL_VERSION   => SHELL_VERSION
)
port map (
    clk_sys         => clk,
    clk_icap        => clk_icap,
    reset_sys       => reset,

    eci_gt_clk_p_link1  => F_CCPIC_CLK_P_LINK1,
    eci_gt_clk_n_link1  => F_CCPIC_CLK_N_LINK1,

    eci_gt_clk_p_link2  => F_CCPIC_CLK_P_LINK2,
    eci_gt_clk_n_link2  => F_CCPIC_CLK_N_LINK2,

    eci_gt_rx_p_link1   => CCPI_C2F_P_LINK1,
    eci_gt_rx_n_link1   => CCPI_C2F_N_LINK1,
    eci_gt_rx_p_link2   => CCPI_C2F_P_LINK2,
    eci_gt_rx_n_link2   => CCPI_C2F_N_LINK2,

    eci_gt_tx_p_link1   => CCPI_F2C_P_LINK1,
    eci_gt_tx_n_link1   => CCPI_F2C_N_LINK1,
    eci_gt_tx_p_link2   => CCPI_F2C_P_LINK2,
    eci_gt_tx_n_link2   => CCPI_F2C_N_LINK2,

    link1_in_data           => link1_in_data,
    link1_in_vc_no          => link1_in_vc_no,
    link1_in_we2            => link1_in_we2,
    link1_in_we3            => link1_in_we3,
    link1_in_we4            => link1_in_we4,
    link1_in_we5            => link1_in_we5,
    link1_in_valid          => link1_in_valid,
    link1_in_credit_return  => link1_in_credit_return,
    link1_out_hi_vc         => link1_out.hi,
    link1_out_hi_vc_ready   => link1_out.hi_ready,
    link1_out_lo_vc         => link1_out.lo,
    link1_out_lo_vc_ready   => link1_out.lo_ready,
    link1_out_credit_return => link1_out_credit_return,

    link2_in_data           => link2_in_data,
    link2_in_vc_no          => link2_in_vc_no,
    link2_in_we2            => link2_in_we2,
    link2_in_we3            => link2_in_we3,
    link2_in_we4            => link2_in_we4,
    link2_in_we5            => link2_in_we5,
    link2_in_valid          => link2_in_valid,
    link2_in_credit_return  => link2_in_credit_return,
    link2_out_hi_vc         => link2_out.hi,
    link2_out_hi_vc_ready   => link2_out.hi_ready,
    link2_out_lo_vc         => link2_out.lo,
    link2_out_lo_vc_ready   => link2_out.lo_ready,
    link2_out_credit_return => link2_out_credit_return,

    link_up                 => eci_link_up,
    link1_link_up           => link1_eci_link_up,
    link2_link_up           => link2_eci_link_up,

    disable_2nd_link        => disable_2nd_link,

    -- AXI Lite master interface for IO addr space
    -- FPGA -> CPU
    m_io_axil_awaddr  => m_io_axil_link.awaddr,
    m_io_axil_awvalid => m_io_axil_link.awvalid,
    m_io_axil_awready => m_io_axil_link.awready,
    m_io_axil_wdata   => m_io_axil_link.wdata,
    m_io_axil_wstrb   => m_io_axil_link.wstrb,
    m_io_axil_wvalid  => m_io_axil_link.wvalid,
    m_io_axil_wready  => m_io_axil_link.wready,
    m_io_axil_bresp   => m_io_axil_link.bresp,
    m_io_axil_bvalid  => m_io_axil_link.bvalid,
    m_io_axil_bready  => m_io_axil_link.bready,
    m_io_axil_araddr  => m_io_axil_link.araddr,
    m_io_axil_arvalid => m_io_axil_link.arvalid,
    m_io_axil_arready => m_io_axil_link.arready,
    m_io_axil_rdata   => m_io_axil_link.rdata,
    m_io_axil_rresp   => m_io_axil_link.rresp,
    m_io_axil_rvalid  => m_io_axil_link.rvalid,
    m_io_axil_rready  => m_io_axil_link.rready,

    -- AXI Lite master interface for IO addr space
    -- CPU -> FPGA
    s_io_axil_awaddr  => s_io_axil_link.awaddr,
    s_io_axil_awvalid => s_io_axil_link.awvalid,
    s_io_axil_awready => s_io_axil_link.awready,
    s_io_axil_wdata   => s_io_axil_link.wdata,
    s_io_axil_wstrb   => s_io_axil_link.wstrb,
    s_io_axil_wvalid  => s_io_axil_link.wvalid,
    s_io_axil_wready  => s_io_axil_link.wready,
    s_io_axil_bresp   => s_io_axil_link.bresp,
    s_io_axil_bvalid  => s_io_axil_link.bvalid,
    s_io_axil_bready  => s_io_axil_link.bready,
    s_io_axil_araddr  => s_io_axil_link.araddr,
    s_io_axil_arvalid => s_io_axil_link.arvalid,
    s_io_axil_arready => s_io_axil_link.arready,
    s_io_axil_rdata   => s_io_axil_link.rdata,
    s_io_axil_rresp   => s_io_axil_link.rresp,
    s_io_axil_rvalid  => s_io_axil_link.rvalid,
    s_io_axil_rready  => s_io_axil_link.rready,

    m_icap_axi_awaddr   => icap_axil_link.awaddr,
    m_icap_axi_awvalid  => icap_axil_link.awvalid,
    m_icap_axi_awready  => icap_axil_link.awready,

    m_icap_axi_wdata    => icap_axil_link.wdata,
    m_icap_axi_wstrb    => icap_axil_link.wstrb,
    m_icap_axi_wvalid   => icap_axil_link.wvalid,
    m_icap_axi_wready   => icap_axil_link.wready,

    m_icap_axi_bresp    => icap_axil_link.bresp,
    m_icap_axi_bvalid   => icap_axil_link.bvalid,
    m_icap_axi_bready   => icap_axil_link.bready,

    m_icap_axi_araddr   => icap_axil_link.araddr,
    m_icap_axi_arvalid  => icap_axil_link.arvalid,
    m_icap_axi_arready  => icap_axil_link.arready,

    m_icap_axi_rdata    => icap_axil_link.rdata,
    m_icap_axi_rresp    => icap_axil_link.rresp,
    m_icap_axi_rvalid   => icap_axil_link.rvalid,
    m_icap_axi_rready   => icap_axil_link.rready,

    shell_status_reg    => shell_status_reg,
    shell_control_reg   => shell_control_reg,

    gpo_regs            => gpo_regs,
    gpi_regs            => gpi_regs
);

i_app : enzian_app
port map (
    clk_sys         => clk,

    prgc0_clk_p     => F_PRGC0_CLK_P,
    prgc0_clk_n     => F_PRGC0_CLK_N,
    prgc1_clk_p     => F_PRGC1_CLK_P,
    prgc1_clk_n     => F_PRGC1_CLK_N,

    reset_sys       => reset_app,
    link1_up        => link1_eci_link_up,
    link2_up        => link2_eci_link_up,

    link1_in_data(63 downto 0)     => link1_in_data(0),
    link1_in_data(127 downto 64)   => link1_in_data(1),
    link1_in_data(191 downto 128)  => link1_in_data(2),
    link1_in_data(255 downto 192)  => link1_in_data(3),
    link1_in_data(319 downto 256)  => link1_in_data(4),
    link1_in_data(383 downto 320)  => link1_in_data(5),
    link1_in_data(447 downto 384)  => link1_in_data(6),
    link1_in_vc_no(3 downto 0)     => link1_in_vc_no(0),
    link1_in_vc_no(7 downto 4)     => link1_in_vc_no(1),
    link1_in_vc_no(11 downto 8)    => link1_in_vc_no(2),
    link1_in_vc_no(15 downto 12)   => link1_in_vc_no(3),
    link1_in_vc_no(19 downto 16)   => link1_in_vc_no(4),
    link1_in_vc_no(23 downto 20)   => link1_in_vc_no(5),
    link1_in_vc_no(27 downto 24)   => link1_in_vc_no(6),
    link1_in_we2                   => link1_in_we2,
    link1_in_we3                   => link1_in_we3,
    link1_in_we4                   => link1_in_we4,
    link1_in_we5                   => link1_in_we5,
    link1_in_valid                 => link1_in_valid,
    link1_in_credit_return         => link1_in_credit_return_c,

    link1_out_hi_data(63 downto 0)      => link1_out.hi.data(0),
    link1_out_hi_data(127 downto 64)    => link1_out.hi.data(1),
    link1_out_hi_data(191 downto 128)   => link1_out.hi.data(2),
    link1_out_hi_data(255 downto 192)   => link1_out.hi.data(3),
    link1_out_hi_data(319 downto 256)   => link1_out.hi.data(4),
    link1_out_hi_data(383 downto 320)   => link1_out.hi.data(5),
    link1_out_hi_data(447 downto 384)   => link1_out.hi.data(6),
    link1_out_hi_data(511 downto 448)   => link1_out.hi.data(7),
    link1_out_hi_data(575 downto 512)   => link1_out.hi.data(8),
    link1_out_hi_vc_no                  => link1_out.hi.vc_no,
    link1_out_hi_size                   => link1_out.hi.size,
    link1_out_hi_valid                  => link1_out_hi_valid_c,
    link1_out_hi_ready                  => link1_out.hi_ready,
    link1_out_lo_data                   => link1_out.lo.data(0),
    link1_out_lo_vc_no                  => link1_out.lo.vc_no,
    link1_out_lo_valid                  => link1_out_lo_valid_c,
    link1_out_lo_ready                  => link1_out.lo_ready,
    link1_out_credit_return             => link1_out_credit_return,

    link2_in_data(63 downto 0)     => link2_in_data(0),
    link2_in_data(127 downto 64)   => link2_in_data(1),
    link2_in_data(191 downto 128)  => link2_in_data(2),
    link2_in_data(255 downto 192)  => link2_in_data(3),
    link2_in_data(319 downto 256)  => link2_in_data(4),
    link2_in_data(383 downto 320)  => link2_in_data(5),
    link2_in_data(447 downto 384)  => link2_in_data(6),
    link2_in_vc_no(3 downto 0)     => link2_in_vc_no(0),
    link2_in_vc_no(7 downto 4)     => link2_in_vc_no(1),
    link2_in_vc_no(11 downto 8)    => link2_in_vc_no(2),
    link2_in_vc_no(15 downto 12)   => link2_in_vc_no(3),
    link2_in_vc_no(19 downto 16)   => link2_in_vc_no(4),
    link2_in_vc_no(23 downto 20)   => link2_in_vc_no(5),
    link2_in_vc_no(27 downto 24)   => link2_in_vc_no(6),
    link2_in_we2                   => link2_in_we2,
    link2_in_we3                   => link2_in_we3,
    link2_in_we4                   => link2_in_we4,
    link2_in_we5                   => link2_in_we5,
    link2_in_valid                 => link2_in_valid,
    link2_in_credit_return         => link2_in_credit_return_c,

    link2_out_hi_data(63 downto 0)      => link2_out.hi.data(0),
    link2_out_hi_data(127 downto 64)    => link2_out.hi.data(1),
    link2_out_hi_data(191 downto 128)   => link2_out.hi.data(2),
    link2_out_hi_data(255 downto 192)   => link2_out.hi.data(3),
    link2_out_hi_data(319 downto 256)   => link2_out.hi.data(4),
    link2_out_hi_data(383 downto 320)   => link2_out.hi.data(5),
    link2_out_hi_data(447 downto 384)   => link2_out.hi.data(6),
    link2_out_hi_data(511 downto 448)   => link2_out.hi.data(7),
    link2_out_hi_data(575 downto 512)   => link2_out.hi.data(8),
    link2_out_hi_vc_no                  => link2_out.hi.vc_no,
    link2_out_hi_size                   => link2_out.hi.size,
    link2_out_hi_valid                  => link2_out_hi_valid_c,
    link2_out_hi_ready                  => link2_out.hi_ready,
    link2_out_lo_data                   => link2_out.lo.data(0),
    link2_out_lo_vc_no                  => link2_out.lo.vc_no,
    link2_out_lo_valid                  => link2_out_lo_valid_c,
    link2_out_lo_ready                  => link2_out.lo_ready,
    link2_out_credit_return             => link2_out_credit_return,

    disable_2nd_link                    => disable_2nd_link,

    s_io_axil_awaddr  => m_io_axil_link.awaddr,
    s_io_axil_awvalid => m_io_axil_link.awvalid,
    s_io_axil_awready => m_io_axil_link.awready,
    s_io_axil_wdata   => m_io_axil_link.wdata,
    s_io_axil_wstrb   => m_io_axil_link.wstrb,
    s_io_axil_wvalid  => m_io_axil_link.wvalid,
    s_io_axil_wready  => m_io_axil_link.wready,
    s_io_axil_bresp   => m_io_axil_link.bresp,
    s_io_axil_bvalid  => m_io_axil_link.bvalid,
    s_io_axil_bready  => m_io_axil_link.bready,
    s_io_axil_araddr  => m_io_axil_link.araddr,
    s_io_axil_arvalid => m_io_axil_link.arvalid,
    s_io_axil_arready => m_io_axil_link.arready,
    s_io_axil_rdata   => m_io_axil_link.rdata,
    s_io_axil_rresp   => m_io_axil_link.rresp,
    s_io_axil_rvalid  => m_io_axil_link.rvalid,
    s_io_axil_rready  => m_io_axil_link.rready,

    m_io_axil_awaddr  => s_io_axil_link.awaddr,
    m_io_axil_awvalid => s_io_axil_link_awvalid_c,
    m_io_axil_awready => s_io_axil_link.awready,
    m_io_axil_wdata   => s_io_axil_link.wdata,
    m_io_axil_wstrb   => s_io_axil_link.wstrb,
    m_io_axil_wvalid  => s_io_axil_link_wvalid_c,
    m_io_axil_wready  => s_io_axil_link.wready,
    m_io_axil_bresp   => s_io_axil_link.bresp,
    m_io_axil_bvalid  => s_io_axil_link.bvalid,
    m_io_axil_bready  => s_io_axil_link.bready,
    m_io_axil_araddr  => s_io_axil_link.araddr,
    m_io_axil_arvalid => s_io_axil_link_arvalid_c,
    m_io_axil_arready => s_io_axil_link.arready,
    m_io_axil_rdata   => s_io_axil_link.rdata,
    m_io_axil_rresp   => s_io_axil_link.rresp,
    m_io_axil_rvalid  => s_io_axil_link.rvalid,
    m_io_axil_rready  => s_io_axil_link.rready,

    s_bscan_bscanid_en => m0_bscan.bscanid_en,
    s_bscan_capture => m0_bscan.capture,
    s_bscan_drck => m0_bscan.drck,
    s_bscan_reset => m0_bscan.reset,
    s_bscan_runtest => m0_bscan.runtest,
    s_bscan_sel => m0_bscan.sel,
    s_bscan_shift => m0_bscan.shift,
    s_bscan_tck => m0_bscan.tck,
    s_bscan_tdi => m0_bscan.tdi,
    s_bscan_tdo => m0_bscan.tdo,
    s_bscan_tms => m0_bscan.tms,
    s_bscan_update => m0_bscan.update,

    mdm_SYS_Rst => mdm_port.SYS_Rst,
    mdm_Clk     => mdm_port.Clk,
    mdm_TDI     => mdm_port.TDI,
    mdm_TDO     => mdm_port.TDO,
    mdm_Reg_En  => mdm_port.Reg_en,
    mdm_Capture => mdm_port.Capture,
    mdm_Shift   => mdm_port.Shift,
    mdm_Update  => mdm_port.Update,
    mdm_Rst     => mdm_port.Rst,
    mdm_Disable => mdm_port.Disable,

    gpo_reg0            => gpo_regs(0),
    gpo_reg1            => gpo_regs(1),
    gpo_reg2            => gpo_regs(2),
    gpo_reg3            => gpo_regs(3),
    gpo_reg4            => gpo_regs(4),
    gpo_reg5            => gpo_regs(5),
    gpo_reg6            => gpo_regs(6),
    gpo_reg7            => gpo_regs(7),
    gpo_reg8            => gpo_regs(8),
    gpo_reg9            => gpo_regs(9),
    gpo_reg10           => gpo_regs(10),
    gpo_reg11           => gpo_regs(11),
    gpo_reg12           => gpo_regs(12),
    gpo_reg13           => gpo_regs(13),
    gpo_reg14           => gpo_regs(14),
    gpo_reg15           => gpo_regs(15),
    gpi_reg0            => gpi_regs(0),
    gpi_reg1            => gpi_regs(1),
    gpi_reg2            => gpi_regs(2),
    gpi_reg3            => gpi_regs(3),
    gpi_reg4            => gpi_regs(4),
    gpi_reg5            => gpi_regs(5),
    gpi_reg6            => gpi_regs(6),
    gpi_reg7            => gpi_regs(7),
    gpi_reg8            => gpi_regs(8),
    gpi_reg9            => gpi_regs(9),
    gpi_reg10           => gpi_regs(10),
    gpi_reg11           => gpi_regs(11),
    gpi_reg12           => gpi_regs(12),
    gpi_reg13           => gpi_regs(13),
    gpi_reg14           => gpi_regs(14),
    gpi_reg15           => gpi_regs(15),

    -- DDR4
    F_D1_ACT_N => F_D1_ACT_N,
    F_D1_A => F_D1_A,
    F_D1_BA => F_D1_BA,
    F_D1_BG => F_D1_BG,
    F_D1_CK_N => F_D1_CK_N,
    F_D1_CK_P => F_D1_CK_P,
    F_D1_CKE => F_D1_CKE,
    F_D1_CS_N => F_D1_CS_N,
    F_D1_DQ => F_D1_DQ,
    F_D1_DQS_N => F_D1_DQS_N,
    F_D1_DQS_P => F_D1_DQS_P,
    F_D1_ODT => F_D1_ODT,
    F_D1_PARITY_N => F_D1_PARITY_N,
    F_D1_RESET_N => F_D1_RESET_N,
    F_D1C_CLK_N => F_D1C_CLK_N,
    F_D1C_CLK_P => F_D1C_CLK_P,

    F_D2_ACT_N => F_D2_ACT_N,
    F_D2_A => F_D2_A,
    F_D2_BA => F_D2_BA,
    F_D2_BG => F_D2_BG,
    F_D2_CK_N => F_D2_CK_N,
    F_D2_CK_P => F_D2_CK_P,
    F_D2_CKE => F_D2_CKE,
    F_D2_CS_N => F_D2_CS_N,
    F_D2_DQ => F_D2_DQ,
    F_D2_DQS_N => F_D2_DQS_N,
    F_D2_DQS_P => F_D2_DQS_P,
    F_D2_ODT => F_D2_ODT,
    F_D2_PARITY_N => F_D2_PARITY_N,
    F_D2_RESET_N => F_D2_RESET_N,
    F_D2C_CLK_N => F_D2C_CLK_N,
    F_D2C_CLK_P => F_D2C_CLK_P,

    F_D3_ACT_N => F_D3_ACT_N,
    F_D3_A => F_D3_A,
    F_D3_BA => F_D3_BA,
    F_D3_BG => F_D3_BG,
    F_D3_CK_N => F_D3_CK_N,
    F_D3_CK_P => F_D3_CK_P,
    F_D3_CKE => F_D3_CKE,
    F_D3_CS_N => F_D3_CS_N,
    F_D3_DQ => F_D3_DQ,
    F_D3_DQS_N => F_D3_DQS_N,
    F_D3_DQS_P => F_D3_DQS_P,
    F_D3_ODT => F_D3_ODT,
    F_D3_PARITY_N => F_D3_PARITY_N,
    F_D3_RESET_N => F_D3_RESET_N,
    F_D3C_CLK_N => F_D3C_CLK_N,
    F_D3C_CLK_P => F_D3C_CLK_P,

    F_D4_ACT_N => F_D4_ACT_N,
    F_D4_A => F_D4_A,
    F_D4_BA => F_D4_BA,
    F_D4_BG => F_D4_BG,
    F_D4_CK_N => F_D4_CK_N,
    F_D4_CK_P => F_D4_CK_P,
    F_D4_CKE => F_D4_CKE,
    F_D4_CS_N => F_D4_CS_N,
    F_D4_DQ => F_D4_DQ,
    F_D4_DQS_N => F_D4_DQS_N,
    F_D4_DQS_P => F_D4_DQS_P,
    F_D4_ODT => F_D4_ODT,
    F_D4_PARITY_N => F_D4_PARITY_N,
    F_D4_RESET_N => F_D4_RESET_N,
    F_D4C_CLK_N => F_D4C_CLK_N,
    F_D4C_CLK_P => F_D4C_CLK_P,

    F_MAC0C_CLK_P   => F_MAC0C_CLK_P,
    F_MAC0C_CLK_N   => F_MAC0C_CLK_N,
    F_MAC0_TX_P     => F_MAC0_TX_P,
    F_MAC0_TX_N     => F_MAC0_TX_N,
    F_MAC0_RX_P     => F_MAC0_RX_P,
    F_MAC0_RX_N     => F_MAC0_RX_N,

    F_MAC1C_CLK_P   => F_MAC1C_CLK_P,
    F_MAC1C_CLK_N   => F_MAC1C_CLK_N,
    F_MAC1_TX_P     => F_MAC1_TX_P,
    F_MAC1_TX_N     => F_MAC1_TX_N,
    F_MAC1_RX_P     => F_MAC1_RX_P,
    F_MAC1_RX_N     => F_MAC1_RX_N,

    F_MAC2C_CLK_P   => F_MAC2C_CLK_P,
    F_MAC2C_CLK_N   => F_MAC2C_CLK_N,
    F_MAC2_TX_P     => F_MAC2_TX_P,
    F_MAC2_TX_N     => F_MAC2_TX_N,
    F_MAC2_RX_P     => F_MAC2_RX_P,
    F_MAC2_RX_N     => F_MAC2_RX_N,

    F_MAC3C_CLK_P   => F_MAC3C_CLK_P,
    F_MAC3C_CLK_N   => F_MAC3C_CLK_N,
    F_MAC3_TX_P     => F_MAC3_TX_P,
    F_MAC3_TX_N     => F_MAC3_TX_N,
    F_MAC3_RX_P     => F_MAC3_RX_P,
    F_MAC3_RX_N     => F_MAC3_RX_N,

    F_PCIE16C_CLK_P => F_PCIE16C_CLK_P,
    F_PCIE16C_CLK_N => F_PCIE16C_CLK_N,
    F_PCIE16_TX_P   => F_PCIE16_TX_P,
    F_PCIE16_TX_N   => F_PCIE16_TX_N,
    F_PCIE16_RX_P   => F_PCIE16_RX_P,
    F_PCIE16_RX_N   => F_PCIE16_RX_N,

    F_NVMEC_CLK_P   => F_NVMEC_CLK_P,
    F_NVMEC_CLK_N   => F_NVMEC_CLK_N,
    F_NVME_TX_P     => F_NVME_TX_P,
    F_NVME_TX_N     => F_NVME_TX_N,
    F_NVME_RX_P     => F_NVME_RX_P,
    F_NVME_RX_N     => F_NVME_RX_N,

    B_C2CC_CLK_P    => B_C2CC_CLK_P,
    B_C2CC_CLK_N    => B_C2CC_CLK_N,
    B_C2C_TX_P      => B_C2C_TX_P,
    B_C2C_TX_N      => B_C2C_TX_N,
    B_C2C_RX_P      => B_C2C_RX_P,
    B_C2C_RX_N      => B_C2C_RX_N,
    B_C2C_NMI       => B_C2C_NMI,

    F_I2C0_SDA      => F_I2C0_SDA,
    F_I2C0_SCL      => F_I2C0_SCL,

    F_I2C1_SDA      => F_I2C1_SDA,
    F_I2C1_SCL      => F_I2C1_SCL,

    F_I2C2_SDA      => F_I2C2_SDA,
    F_I2C2_SCL      => F_I2C2_SCL,

    F_I2C3_SDA      => F_I2C3_SDA,
    F_I2C3_SCL      => F_I2C3_SCL,

    F_I2C4_SDA      => F_I2C4_SDA,
    F_I2C4_SCL      => F_I2C4_SCL,

    F_I2C5_SDA      => F_I2C5_SDA,
    F_I2C5_SCL      => F_I2C5_SCL,
    F_I2C5_RESET_N  => F_I2C5_RESET_N,
    F_I2C5_INT_N    => F_I2C5_INT_N,

    B_FUART_TXD     => B_FUART_TXD,
    B_FUART_RXD     => B_FUART_RXD,
    B_FUART_RTS     => B_FUART_RTS,
    B_FUART_CTS     => B_FUART_CTS,

    F_IRQ_IRQ0      => F_IRQ_IRQ0,
    F_IRQ_IRQ1      => F_IRQ_IRQ1,
    F_IRQ_IRQ2      => F_IRQ_IRQ2,
    F_IRQ_IRQ3      => F_IRQ_IRQ3
);

i_icap : axi_hwicap_0
port map (
    icap_clk        => clk_icap,
    eos_in          => '0',
    s_axi_aclk      => clk,
    s_axi_aresetn   => reset_n,

    s_axi_awaddr    => icap_axil_link.awaddr,
    s_axi_awvalid   => icap_axil_link.awvalid,
    s_axi_awready   => icap_axil_link.awready,

    s_axi_wdata     => icap_axil_link.wdata,
    s_axi_wstrb     => icap_axil_link.wstrb,
    s_axi_wvalid    => icap_axil_link.wvalid,
    s_axi_wready    => icap_axil_link.wready,

    s_axi_bresp     => icap_axil_link.bresp,
    s_axi_bvalid    => icap_axil_link.bvalid,
    s_axi_bready    => icap_axil_link.bready,

    s_axi_araddr    => icap_axil_link.araddr,
    s_axi_arvalid   => icap_axil_link.arvalid,
    s_axi_arready   => icap_axil_link.arready,

    s_axi_rdata     => icap_axil_link.rdata,
    s_axi_rresp     => icap_axil_link.rresp,
    s_axi_rvalid    => icap_axil_link.rvalid,
    s_axi_rready    => icap_axil_link.rready
);

i_debug_bridge : debug_bridge_static
port map (
    m0_bscan_bscanid_en => m0_bscan.bscanid_en,
    m0_bscan_capture => m0_bscan.capture,
    m0_bscan_drck => m0_bscan.drck,
    m0_bscan_reset => m0_bscan.reset,
    m0_bscan_runtest => m0_bscan.runtest,
    m0_bscan_sel => m0_bscan.sel,
    m0_bscan_shift => m0_bscan.shift,
    m0_bscan_tck => m0_bscan.tck,
    m0_bscan_tdi => m0_bscan.tdi,
    m0_bscan_tdo => m0_bscan.tdo,
    m0_bscan_tms => m0_bscan.tms,
    m0_bscan_update => m0_bscan.update,
    m1_bscan_bscanid_en => m1_bscan.bscanid_en,
    m1_bscan_capture => m1_bscan.capture,
    m1_bscan_drck => m1_bscan.drck,
    m1_bscan_reset => m1_bscan.reset,
    m1_bscan_runtest => m1_bscan.runtest,
    m1_bscan_sel => m1_bscan.sel,
    m1_bscan_shift => m1_bscan.shift,
    m1_bscan_tck => m1_bscan.tck,
    m1_bscan_tdi => m1_bscan.tdi,
    m1_bscan_tdo => m1_bscan.tdo,
    m1_bscan_tms => m1_bscan.tms,
    m1_bscan_update => m1_bscan.update
);

i_debug_hub : debug_hub_static
port map (
    clk                 => clk_icap,
    s_bscan_bscanid_en => m1_bscan.bscanid_en,
    s_bscan_capture => m1_bscan.capture,
    s_bscan_drck => m1_bscan.drck,
    s_bscan_reset => m1_bscan.reset,
    s_bscan_runtest => m1_bscan.runtest,
    s_bscan_sel => m1_bscan.sel,
    s_bscan_shift => m1_bscan.shift,
    s_bscan_tck => m1_bscan.tck,
    s_bscan_tdi => m1_bscan.tdi,
    s_bscan_tdo => m1_bscan.tdo,
    s_bscan_tms => m1_bscan.tms,
    s_bscan_update => m1_bscan.update
);

-- Microblaze Debug Module
-- It works better with Vitis when MDM is in the static region
i_mdm : mdm_0
port map (
    Debug_SYS_Rst   => mdm_port.SYS_Rst,
    Dbg_Clk_0       => mdm_port.Clk,
    Dbg_TDI_0       => mdm_port.TDI,
    Dbg_TDO_0       => mdm_port.TDO,
    Dbg_Reg_En_0    => mdm_port.Reg_En,
    Dbg_Capture_0   => mdm_port.Capture,
    Dbg_Shift_0     => mdm_port.Shift,
    Dbg_Update_0    => mdm_port.Update,
    Dbg_Rst_0       => mdm_port.Rst,
    Dbg_Disable_0   => mdm_port.Disable
);

end behavioural;
