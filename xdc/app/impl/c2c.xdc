# Chip-2-Chip (FPGA-2-BMC) link

set_property PACKAGE_PIN AW9 [get_ports B_C2CC_CLK_P]
set_property PACKAGE_PIN AW8 [get_ports B_C2CC_CLK_N]

set_property PACKAGE_PIN AU25 [get_ports B_C2C_NMI]

set_property PACKAGE_PIN BF5 [get_ports {B_C2C_RX_P[0]}]
set_property PACKAGE_PIN BF4 [get_ports {B_C2C_RX_N[0]}]
set_property PACKAGE_PIN BC2 [get_ports {B_C2C_TX_P[0]}]
set_property PACKAGE_PIN BC1 [get_ports {B_C2C_TX_N[0]}]
