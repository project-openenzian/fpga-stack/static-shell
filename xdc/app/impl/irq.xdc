# CPU IRQs lines

set_property PACKAGE_PIN BE25 [get_ports F_IRQ_IRQ0]
set_property PACKAGE_PIN BF25 [get_ports F_IRQ_IRQ1]
set_property PACKAGE_PIN BC26 [get_ports F_IRQ_IRQ2]
set_property PACKAGE_PIN BC27 [get_ports F_IRQ_IRQ3]
