# FPGA UART port

set_property PACKAGE_PIN BB27 [get_ports B_FUART_RXD]
set_property PACKAGE_PIN BB26 [get_ports B_FUART_TXD]
set_property PACKAGE_PIN BB25 [get_ports B_FUART_CTS]
set_property PACKAGE_PIN BA25 [get_ports B_FUART_RTS]
