# 100G MACs

set_property PACKAGE_PIN AV38 [get_ports F_MAC0C_CLK_P]
set_property PACKAGE_PIN AV39 [get_ports F_MAC0C_CLK_N]
set_property PACKAGE_PIN AG36 [get_ports F_MAC1C_CLK_P]
set_property PACKAGE_PIN AG37 [get_ports F_MAC1C_CLK_N]
set_property PACKAGE_PIN AD11 [get_ports F_MAC2C_CLK_P]
set_property PACKAGE_PIN AD10 [get_ports F_MAC2C_CLK_N]
set_property PACKAGE_PIN D11 [get_ports F_MAC3C_CLK_P]
set_property PACKAGE_PIN D10 [get_ports F_MAC3C_CLK_N]

set_property PACKAGE_PIN AU40 [get_ports {F_MAC0_TX_P[0]}]
set_property PACKAGE_PIN AU41 [get_ports {F_MAC0_TX_N[0]}]
set_property PACKAGE_PIN AU45 [get_ports {F_MAC0_RX_P[0]}]
set_property PACKAGE_PIN AU46 [get_ports {F_MAC0_RX_N[0]}]
set_property PACKAGE_PIN AT38 [get_ports {F_MAC0_TX_P[1]}]
set_property PACKAGE_PIN AT39 [get_ports {F_MAC0_TX_N[1]}]
set_property PACKAGE_PIN AT43 [get_ports {F_MAC0_RX_P[1]}]
set_property PACKAGE_PIN AT44 [get_ports {F_MAC0_RX_N[1]}]
set_property PACKAGE_PIN AR40 [get_ports {F_MAC0_TX_P[2]}]
set_property PACKAGE_PIN AR41 [get_ports {F_MAC0_TX_N[2]}]
set_property PACKAGE_PIN AR45 [get_ports {F_MAC0_RX_P[2]}]
set_property PACKAGE_PIN AR46 [get_ports {F_MAC0_RX_N[2]}]
set_property PACKAGE_PIN AP38 [get_ports {F_MAC0_TX_P[3]}]
set_property PACKAGE_PIN AP39 [get_ports {F_MAC0_TX_N[3]}]
set_property PACKAGE_PIN AP43 [get_ports {F_MAC0_RX_P[3]}]
set_property PACKAGE_PIN AP44 [get_ports {F_MAC0_RX_N[3]}]

set_property PACKAGE_PIN AE40 [get_ports {F_MAC1_TX_P[0]}]
set_property PACKAGE_PIN AE41 [get_ports {F_MAC1_TX_N[0]}]
set_property PACKAGE_PIN AE45 [get_ports {F_MAC1_RX_P[0]}]
set_property PACKAGE_PIN AE46 [get_ports {F_MAC1_RX_N[0]}]
set_property PACKAGE_PIN AD38 [get_ports {F_MAC1_TX_P[1]}]
set_property PACKAGE_PIN AD39 [get_ports {F_MAC1_TX_N[1]}]
set_property PACKAGE_PIN AD43 [get_ports {F_MAC1_RX_P[1]}]
set_property PACKAGE_PIN AD44 [get_ports {F_MAC1_RX_N[1]}]
set_property PACKAGE_PIN AC40 [get_ports {F_MAC1_TX_P[2]}]
set_property PACKAGE_PIN AC41 [get_ports {F_MAC1_TX_N[2]}]
set_property PACKAGE_PIN AC45 [get_ports {F_MAC1_RX_P[2]}]
set_property PACKAGE_PIN AC46 [get_ports {F_MAC1_RX_N[2]}]
set_property PACKAGE_PIN AB38 [get_ports {F_MAC1_TX_P[3]}]
set_property PACKAGE_PIN AB39 [get_ports {F_MAC1_TX_N[3]}]
set_property PACKAGE_PIN AB43 [get_ports {F_MAC1_RX_P[3]}]
set_property PACKAGE_PIN AB44 [get_ports {F_MAC1_RX_N[3]}]

set_property PACKAGE_PIN AE9 [get_ports {F_MAC2_TX_P[0]}]
set_property PACKAGE_PIN AE8 [get_ports {F_MAC2_TX_N[0]}]
set_property PACKAGE_PIN AE4 [get_ports {F_MAC2_RX_P[0]}]
set_property PACKAGE_PIN AE3 [get_ports {F_MAC2_RX_N[0]}]
set_property PACKAGE_PIN AD7 [get_ports {F_MAC2_TX_P[1]}]
set_property PACKAGE_PIN AD6 [get_ports {F_MAC2_TX_N[1]}]
set_property PACKAGE_PIN AD2 [get_ports {F_MAC2_RX_P[1]}]
set_property PACKAGE_PIN AD1 [get_ports {F_MAC2_RX_N[1]}]
set_property PACKAGE_PIN AC9 [get_ports {F_MAC2_TX_P[2]}]
set_property PACKAGE_PIN AC8 [get_ports {F_MAC2_TX_N[2]}]
set_property PACKAGE_PIN AC4 [get_ports {F_MAC2_RX_P[2]}]
set_property PACKAGE_PIN AC3 [get_ports {F_MAC2_RX_N[2]}]
set_property PACKAGE_PIN AB7 [get_ports {F_MAC2_TX_P[3]}]
set_property PACKAGE_PIN AB6 [get_ports {F_MAC2_TX_N[3]}]
set_property PACKAGE_PIN AB2 [get_ports {F_MAC2_RX_P[3]}]
set_property PACKAGE_PIN AB1 [get_ports {F_MAC2_RX_N[3]}]

set_property PACKAGE_PIN E9 [get_ports {F_MAC3_TX_P[0]}]
set_property PACKAGE_PIN E8 [get_ports {F_MAC3_TX_N[0]}]
set_property PACKAGE_PIN E4 [get_ports {F_MAC3_RX_P[0]}]
set_property PACKAGE_PIN E3 [get_ports {F_MAC3_RX_N[0]}]
set_property PACKAGE_PIN D7 [get_ports {F_MAC3_TX_P[1]}]
set_property PACKAGE_PIN D6 [get_ports {F_MAC3_TX_N[1]}]
set_property PACKAGE_PIN D2 [get_ports {F_MAC3_RX_P[1]}]
set_property PACKAGE_PIN D1 [get_ports {F_MAC3_RX_N[1]}]
set_property PACKAGE_PIN C9 [get_ports {F_MAC3_TX_P[2]}]
set_property PACKAGE_PIN C8 [get_ports {F_MAC3_TX_N[2]}]
set_property PACKAGE_PIN C4 [get_ports {F_MAC3_RX_P[2]}]
set_property PACKAGE_PIN C3 [get_ports {F_MAC3_RX_N[2]}]
set_property PACKAGE_PIN A9 [get_ports {F_MAC3_TX_P[3]}]
set_property PACKAGE_PIN A8 [get_ports {F_MAC3_TX_N[3]}]
set_property PACKAGE_PIN A5 [get_ports {F_MAC3_RX_P[3]}]
set_property PACKAGE_PIN A4 [get_ports {F_MAC3_RX_N[3]}]
