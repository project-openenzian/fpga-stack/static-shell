# Fabric clocks

# Configured to 100MHz
create_clock -period 10.000 -name clk_io [get_ports F_PRGC0_CLK_P]
# Configured to 300MHz
create_clock -period 3.333 -name clk_prgc1 [get_ports F_PRGC1_CLK_P]

set_property IOSTANDARD DIFF_SSTL12 [get_ports {F_PRGC0_CLK_P}]
set_property IOSTANDARD DIFF_SSTL12 [get_ports {F_PRGC0_CLK_N}]
set_property IOSTANDARD DIFF_SSTL12 [get_ports {F_PRGC1_CLK_P}]
set_property IOSTANDARD DIFF_SSTL12 [get_ports {F_PRGC1_CLK_N}]
