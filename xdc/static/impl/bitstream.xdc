# General bitstream configuration

set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.STARTUP.MATCH_CYCLE "NoWait" [current_design]

