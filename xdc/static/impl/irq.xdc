# CPU IRQs lines

set_property IOSTANDARD LVCMOS12 [get_ports F_IRQ_IRQ0]
set_property DRIVE 2 [get_ports F_IRQ_IRQ0]
set_property SLEW SLOW [get_ports F_IRQ_IRQ0]
set_property IOSTANDARD LVCMOS12 [get_ports F_IRQ_IRQ1]
set_property DRIVE 2 [get_ports F_IRQ_IRQ1]
set_property SLEW SLOW [get_ports F_IRQ_IRQ1]
set_property IOSTANDARD LVCMOS12 [get_ports F_IRQ_IRQ2]
set_property DRIVE 2 [get_ports F_IRQ_IRQ2]
set_property SLEW SLOW [get_ports F_IRQ_IRQ2]
set_property IOSTANDARD LVCMOS12 [get_ports F_IRQ_IRQ3]
set_property DRIVE 2 [get_ports F_IRQ_IRQ3]
set_property SLEW SLOW [get_ports F_IRQ_IRQ3]
