# FPGA UART port

set_property IOSTANDARD LVCMOS12 [get_ports B_FUART_RXD]
set_property DRIVE 2 [get_ports B_FUART_RXD]
set_property SLEW SLOW [get_ports B_FUART_RXD]
set_property IOSTANDARD LVCMOS12 [get_ports B_FUART_TXD]
set_property IOSTANDARD LVCMOS12 [get_ports B_FUART_CTS]
set_property DRIVE 2 [get_ports B_FUART_CTS]
set_property SLEW SLOW [get_ports B_FUART_CTS]
set_property IOSTANDARD LVCMOS12 [get_ports B_FUART_RTS]
