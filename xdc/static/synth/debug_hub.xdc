set_property C_CLK_INPUT_FREQ_HZ 107421875 [get_debug_cores i_debug_hub/U0/xsdbm]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores i_debug_hub/U0/xsdbm]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores i_debug_hub/U0/xsdbm]
connect_debug_port i_debug_hub/U0/xsdbm/clk [get_nets i_debug_hub/U0/clk]
