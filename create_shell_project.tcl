#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

set project     "shell"
set part        "xcvu9p-flgb2104-3-e"
#set board_part  "eth.ch:enzian:1.0"

# IP location must be passed as the first argument.
set ip_dir [lindex $argv 0]

# Source files are included relative to the directory containing this script.
set shell_src_dir   [file normalize "[file dirname [info script]]"]
set build_dir [file normalize "."]

# Create project
create_project "${project}" "${build_dir}/${project}" -part $part
set proj [current_project]

# Set project properties
#set_property "board_part" $board_part                   $proj
set_property "default_lib" "xil_defaultlib"                 $proj
set_property "ip_cache_permissions" "read write"            $proj
set_property "ip_output_repo" "${build_dir}/${project}/${project}.cache/ip"  $proj
set_property "sim.ip.auto_export_scripts" "1"               $proj
set_property "simulator_language" "Mixed"                   $proj
set_property "target_language" "VHDL"                       $proj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY"           $proj
set_property "ip_repo_paths" "$ip_dir"                      $proj

# Make sure any repository IP is visible.
update_ip_catalog

add_files -fileset [get_filesets sources_1] "${shell_src_dir}/hdl"
add_files -fileset [get_filesets sources_1] "${shell_src_dir}/eci-transport/hdl"
add_files -fileset [get_filesets sources_1] "${shell_src_dir}/eci-toolkit/hdl"
# Used only in synthesis
add_files -fileset [get_filesets constrs_1] "${shell_src_dir}/xdc/static/synth"
set_property used_in_implementation false [get_files -of_objects [get_filesets constrs_1]]

# Set top entity
set_property "top" "enzian_shell" [get_filesets sources_1]

# Create a project-local constraint file to take debugging constraints that we
# don't want to propagate to the repository.
file mkdir "${build_dir}/${project}/${project}.srcs/constrs_1"
close [ open "${build_dir}/${project}/${project}.srcs/constrs_1/local.xdc" w ]

### Regenerate IP
puts "regenerate IPs"

# Generate ECI transceivers
# Instantiate link 1 transceivers (0-11)
create_ip -name gtwizard_ultrascale -vendor xilinx.com -library ip \
          -module_name xcvr_link1
set_property -dict [list \
    CONFIG.preset {GTY-10GBASE-KR}] [get_ips xcvr_link1]

set_property -dict [list \
CONFIG.CHANNEL_ENABLE {X1Y35 X1Y34 X1Y33 X1Y32 \
                        X1Y31 X1Y30 X1Y29 X1Y28 \
                        X1Y27 X1Y26 X1Y25 X1Y24} \
CONFIG.TX_MASTER_CHANNEL {X1Y29} \
CONFIG.RX_MASTER_CHANNEL {X1Y29} \
CONFIG.TX_LINE_RATE {10.3125} \
CONFIG.TX_REFCLK_FREQUENCY {156.25} \
CONFIG.RX_BUFFER_MODE {0} \
CONFIG.RX_LINE_RATE {10.3125} \
CONFIG.RX_REFCLK_FREQUENCY {156.25} \
CONFIG.RX_JTOL_FC {3.7492501} \
CONFIG.RX_EQ_MODE {LPM} \
CONFIG.RX_TERMINATION {FLOAT} \
CONFIG.INS_LOSS_NYQ {3} \
CONFIG.TX_BUFFER_MODE {0} \
CONFIG.TX_DATA_ENCODING {64B67B} \
CONFIG.TX_DIFF_SWING_EMPH_MODE {CUSTOM} \
CONFIG.TX_USER_DATA_WIDTH {64} \
CONFIG.TX_INT_DATA_WIDTH {32} \
CONFIG.RX_DATA_DECODING {64B67B} \
CONFIG.RX_USER_DATA_WIDTH {64} \
CONFIG.RX_INT_DATA_WIDTH {32} \
CONFIG.RX_CB_MAX_LEVEL {6} \
CONFIG.ENABLE_OPTIONAL_PORTS {txdiffctrl_in} \
CONFIG.LOCATE_TX_USER_CLOCKING {EXAMPLE_DESIGN} \
CONFIG.LOCATE_RX_USER_CLOCKING {EXAMPLE_DESIGN} \
CONFIG.FREERUN_FREQUENCY {156.25}] \
[get_ips xcvr_link1]

generate_target all [get_ips xcvr_link1]

# Instantiate link 2 transceivers (12-23)
create_ip -name gtwizard_ultrascale -vendor xilinx.com -library ip \
          -module_name xcvr_link2
set_property -dict [list \
    CONFIG.preset {GTY-10GBASE-KR}] [get_ips xcvr_link2]


set_property -dict [list \
CONFIG.CHANNEL_ENABLE {X1Y55 X1Y54 X1Y53 X1Y52 \
                        X1Y51 X1Y50 X1Y49 X1Y48 \
                        X1Y47 X1Y46 X1Y45 X1Y44} \
CONFIG.TX_MASTER_CHANNEL {X1Y49} \
CONFIG.RX_MASTER_CHANNEL {X1Y49} \
CONFIG.TX_LINE_RATE {10.3125} \
CONFIG.TX_REFCLK_FREQUENCY {156.25} \
CONFIG.RX_BUFFER_MODE {0} \
CONFIG.RX_LINE_RATE {10.3125} \
CONFIG.RX_REFCLK_FREQUENCY {156.25} \
CONFIG.RX_JTOL_FC {3.7492501} \
CONFIG.RX_EQ_MODE {LPM} \
CONFIG.RX_TERMINATION {FLOAT} \
CONFIG.INS_LOSS_NYQ {3} \
CONFIG.TX_BUFFER_MODE {0} \
CONFIG.TX_DATA_ENCODING {64B67B} \
CONFIG.TX_DIFF_SWING_EMPH_MODE {CUSTOM} \
CONFIG.TX_USER_DATA_WIDTH {64} \
CONFIG.TX_INT_DATA_WIDTH {32} \
CONFIG.RX_DATA_DECODING {64B67B} \
CONFIG.RX_USER_DATA_WIDTH {64} \
CONFIG.RX_INT_DATA_WIDTH {32} \
CONFIG.RX_CB_MAX_LEVEL {6} \
CONFIG.ENABLE_OPTIONAL_PORTS {txdiffctrl_in} \
CONFIG.LOCATE_TX_USER_CLOCKING {EXAMPLE_DESIGN} \
CONFIG.LOCATE_RX_USER_CLOCKING {EXAMPLE_DESIGN} \
CONFIG.FREERUN_FREQUENCY {156.25}] \
[get_ips xcvr_link2]

generate_target all [get_ips xcvr_link2]

# ILA for ECI edge signals
create_ip -name ila -vendor xilinx.com -library ip \
              -module_name ila_eci_edge
set_property -dict [list \
    CONFIG.C_NUM_OF_PROBES {52} \
    CONFIG.C_DATA_DEPTH {1024} \
    CONFIG.C_EN_STRG_QUAL {0} \
    CONFIG.C_ADV_TRIGGER {false} \
    CONFIG.C_INPUT_PIPE_STAGES {0} \
    CONFIG.C_PROBE0_WIDTH {1} \
    CONFIG.C_PROBE1_WIDTH {6} \
    CONFIG.C_PROBE2_WIDTH {1} \
    CONFIG.C_PROBE3_WIDTH {1} \
    CONFIG.C_PROBE4_WIDTH {1} \
    CONFIG.C_PROBE5_WIDTH {1} \
    CONFIG.C_PROBE6_WIDTH {40} \
    CONFIG.C_PROBE6_TYPE {1} \
    CONFIG.C_PROBE7_WIDTH {64} \
    CONFIG.C_PROBE7_TYPE {1} \
    CONFIG.C_PROBE8_WIDTH {64} \
    CONFIG.C_PROBE8_TYPE {1} \
    CONFIG.C_PROBE9_WIDTH {64} \
    CONFIG.C_PROBE9_TYPE {1} \
    CONFIG.C_PROBE10_WIDTH {64} \
    CONFIG.C_PROBE10_TYPE {1} \
    CONFIG.C_PROBE11_WIDTH {64} \
    CONFIG.C_PROBE11_TYPE {1} \
    CONFIG.C_PROBE12_WIDTH {64} \
    CONFIG.C_PROBE12_TYPE {1} \
    CONFIG.C_PROBE13_WIDTH {64} \
    CONFIG.C_PROBE13_TYPE {1} \
    CONFIG.C_PROBE14_WIDTH {1} \
    CONFIG.C_PROBE15_WIDTH {1} \
    CONFIG.C_PROBE16_WIDTH {1} \
    CONFIG.C_PROBE17_WIDTH {1} \
    CONFIG.C_PROBE18_WIDTH {40} \
    CONFIG.C_PROBE18_TYPE {1} \
    CONFIG.C_PROBE19_WIDTH {64} \
    CONFIG.C_PROBE19_TYPE {1} \
    CONFIG.C_PROBE20_WIDTH {64} \
    CONFIG.C_PROBE20_TYPE {1} \
    CONFIG.C_PROBE21_WIDTH {64} \
    CONFIG.C_PROBE21_TYPE {1} \
    CONFIG.C_PROBE22_WIDTH {64} \
    CONFIG.C_PROBE22_TYPE {1} \
    CONFIG.C_PROBE23_WIDTH {64} \
    CONFIG.C_PROBE23_TYPE {1} \
    CONFIG.C_PROBE24_WIDTH {64} \
    CONFIG.C_PROBE24_TYPE {1} \
    CONFIG.C_PROBE25_WIDTH {64} \
    CONFIG.C_PROBE25_TYPE {1} \
    CONFIG.C_PROBE26_WIDTH {1} \
    CONFIG.C_PROBE27_WIDTH {6} \
    CONFIG.C_PROBE28_WIDTH {1} \
    CONFIG.C_PROBE29_WIDTH {1} \
    CONFIG.C_PROBE30_WIDTH {1} \
    CONFIG.C_PROBE31_WIDTH {1} \
    CONFIG.C_PROBE32_WIDTH {40} \
    CONFIG.C_PROBE32_TYPE {1} \
    CONFIG.C_PROBE33_WIDTH {64} \
    CONFIG.C_PROBE33_TYPE {1} \
    CONFIG.C_PROBE34_WIDTH {64} \
    CONFIG.C_PROBE34_TYPE {1} \
    CONFIG.C_PROBE35_WIDTH {64} \
    CONFIG.C_PROBE35_TYPE {1} \
    CONFIG.C_PROBE36_WIDTH {64} \
    CONFIG.C_PROBE36_TYPE {1} \
    CONFIG.C_PROBE37_WIDTH {64} \
    CONFIG.C_PROBE37_TYPE {1} \
    CONFIG.C_PROBE38_WIDTH {64} \
    CONFIG.C_PROBE38_TYPE {1} \
    CONFIG.C_PROBE39_WIDTH {64} \
    CONFIG.C_PROBE39_TYPE {1} \
    CONFIG.C_PROBE40_WIDTH {1} \
    CONFIG.C_PROBE41_WIDTH {1} \
    CONFIG.C_PROBE42_WIDTH {1} \
    CONFIG.C_PROBE43_WIDTH {1} \
    CONFIG.C_PROBE44_WIDTH {40} \
    CONFIG.C_PROBE44_TYPE {1} \
    CONFIG.C_PROBE45_WIDTH {64} \
    CONFIG.C_PROBE45_TYPE {1} \
    CONFIG.C_PROBE46_WIDTH {64} \
    CONFIG.C_PROBE46_TYPE {1} \
    CONFIG.C_PROBE47_WIDTH {64} \
    CONFIG.C_PROBE47_TYPE {1} \
    CONFIG.C_PROBE48_WIDTH {64} \
    CONFIG.C_PROBE48_TYPE {1} \
    CONFIG.C_PROBE49_WIDTH {64} \
    CONFIG.C_PROBE49_TYPE {1} \
    CONFIG.C_PROBE50_WIDTH {64} \
    CONFIG.C_PROBE50_TYPE {1} \
    CONFIG.C_PROBE51_WIDTH {64} \
    CONFIG.C_PROBE51_TYPE {1} \
    CONFIG.ALL_PROBE_SAME_MU {false} \
    ] [get_ips ila_eci_edge]
generate_target all [get_ips ila_eci_edge]

# Debug bridge

create_ip -name debug_bridge -vendor xilinx.com -library ip \
              -module_name debug_bridge_static
set_property -dict [list \
    CONFIG.C_DEBUG_MODE {7} \
    CONFIG.C_DESIGN_TYPE {0} \
    CONFIG.C_NUM_BS_MASTER {2} \
    ] [get_ips debug_bridge_static]
generate_target all [get_ips debug_bridge_static]

create_ip -name debug_bridge -vendor xilinx.com -library ip \
              -module_name debug_hub_static
set_property -dict [list \
    CONFIG.C_DEBUG_MODE {1} \
    CONFIG.C_DESIGN_TYPE {0} \
    CONFIG.C_NUM_BS_MASTER {0} \
    ] [get_ips debug_hub_static]
generate_target all [get_ips debug_hub_static]

create_ip -name axi_hwicap -vendor xilinx.com -library ip \
              -module_name axi_hwicap_0
set_property -dict [list \
    CONFIG.C_ENABLE_ASYNC {1} \
    CONFIG.C_NOREAD {1} \
    CONFIG.C_WRITE_FIFO_DEPTH {1024} \
    ] [get_ips axi_hwicap_0]
generate_target all [get_ips axi_hwicap_0]

create_ip -name mdm -vendor xilinx.com -library ip \
              -module_name mdm_0
generate_target all [get_ips mdm_0]

close_project
