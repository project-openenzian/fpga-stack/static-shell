#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

# Place & route an app linked to a static shell, optimize routing

source -quiet enzian.conf.tcl

if {[info exists enzian_shell] == 0} {
    set enzian_shell "shell"
}
if {[info exists enzian_app] == 0} {
    set enzian_app [lindex $argv 0]
}
if {[info exists build_dir] == 0} {
    set build_dir [file normalize "."]
}
if {[info exists enzian_shell_dir] == 0} {
    if {[info exists env(ENZIAN_SHELL_DIR)] == 1} {
        set enzian_shell_dir $env(ENZIAN_SHELL_DIR)
    } else {
        set enzian_shell_dir $build_dir
    }
}
set shell_src_dir   [file normalize "[file dirname [info script]]"]

source -quiet "${shell_src_dir}/impl_functions.tcl"

open_checkpoint "${build_dir}/${enzian_shell}_${enzian_app}_routed.dcp"

implement_design_opt

write_checkpoint -force "${build_dir}/${enzian_shell}_${enzian_app}_routed.dcp"
report_utilization -file "${build_dir}/${enzian_shell}_${enzian_app}_utilization.rpt"
report_timing_summary -file "${build_dir}/${enzian_shell}_${enzian_app}_timing_summary.rpt"
close_project
