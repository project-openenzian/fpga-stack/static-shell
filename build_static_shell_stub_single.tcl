#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

# Build a shell, a stub and a static image, optimize routing, one thread
set src_dir [file normalize "[file dirname [info script]]"]

# Create a local config file
set conffile [open "enzian.conf.tcl" "w"]
puts $conffile [join [list "set enzian_shell_dir \"" [file normalize "."] "\""] ""]
close $conffile

source "${src_dir}/create_shell_project.tcl"
source "${src_dir}/create_stub_project.tcl"
source "${src_dir}/synth_shell_project.tcl"
source "${src_dir}/synth_app_project.tcl"
source "${src_dir}/impl_static_shell_app_single.tcl"
source "${src_dir}/write_bitstream_app.tcl"
